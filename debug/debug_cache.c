//
//  debug_cache.c
//  odagrun
//
//  Created by Danny Goossen on 25/08/2018.
//  Copyright © 2018 Danny Goossen. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "../src/cJSON_deploy.h"
#include "../src/error.h"
#include "../src/utils.h"
#include "../src/cache.h"
#include "../src/Environment.h"
#include "../src/yaml2cjson.h"
#include "../src/dkr_api.h"
#include "../src/cache.h"
/*
 int parse_git_cache( struct dkr_api_data_s * apidata, cJSON * job,int local_shell);
 int parse_workspaces( struct dkr_api_data_s * apidata, cJSON * job,cJSON * workspaces,int local_shell);
 
 int append_image_2_layer_data( void * apidata,const char* image, cJSON ** layer_data ,int * SchemaVersion,const char * display_name);
 int produce_config(cJSON*layerdata,cJSON ** config);
 int produce_manifest_V2(cJSON*layerdata,cJSON ** manifest,char *config_digest,int config_size);
 int produce_manifest_V1(cJSON*layerdata,cJSON ** manifest,char * name,char*tag);
 
 int calc_thresshold_files(const cJSON * threshold_path_obj, cJSON* job,char ** errormsg);
 int cache_do_push(cJSON * job,struct trace_Struct * trace,const char * CI_PROJECT_DIR, cJSON* localShell);
 void cache_do_clean(cJSON * job, struct trace_Struct *trace,cJSON * localShell);
 */



/*
- name: repo C$DISTRO_RELEASE
  key: x86_64
  scope: global
  path:
   - cache/yum/x86_64/$DISTRO_RELEASE/base
   - cache/yum/x86_64/$DISTRO_RELEASE/updates
  strategy: push-pull
  threshold:
    path:
    - cache/yum/x86_64/$DISTRO_RELEASE/base/packages/*.rpm
    - cache/yum/x86_64/$DISTRO_RELEASE/updates/packages/*.rpm
- name: deployctl $DISTRO_RELEASE
  key: x86_64
  scope: global
  path:
    - cache/yum/x86_64/$DISTRO_RELEASE/deployctl
  strategy: push-pull
  threshold:
    path:
    - cache/yum/x86_64/$DISTRO_RELEASE/deployctl/packages/*.rpm
 */

void oc_api_cleanup(struct oc_api_data_s ** oc_api_data)
{
    return;
}
int oc_api_init(  struct oc_api_data_s ** oc_api_data, const char * ca_cert_bundle, const char * master_url, const char * token)
{
    return 0;
}
int oc_api( struct oc_api_data_s * oc_api_data ,char * methode, cJSON * data_json, cJSON ** result, const char * api, ...)
{
    return 200;
}

int  update_details(void * userp)
{
    return 0;
}
size_t Write_dyn_trace_pad(void *userp,enum term_color color, int len2pad,const char *message, ...)
{
    return 0;
}
size_t Write_dyn_trace(void *userp,enum term_color color,const char *message, ...)
{
    return 0;
}
int create_imagestream(struct oc_api_data_s * oc_api_data,const cJSON * job,const char * oc_image_name)
{
    return 0;
}

void print_api_error(struct trace_Struct* trace,int error)
{
}


int int_registry_push(const cJSON * job,struct trace_Struct * trace,const char * rootfs,char * const *pathnames,size_t pathname_cnt,const char *image,const char * from_image, const cJSON*environment)
{
    cJSON * result=cJSON_GetObjectItem(job, "RESULT");
    if (!result)
    {
        result=cJSON_CreateArray();
        cJSON_AddItemToObject((cJSON*)job, "RESULT", result);
    }
    cJSON*new_result=cJSON_CreateObject();
    cJSON_add_string(new_result, "image",image);
    if (pathnames &&pathname_cnt>0)
    {
        cJSON * pathnames_o=cJSON_CreateArray();
        cJSON_AddItemToObject(new_result, "pathnames", pathnames_o);
        int i;
        for (i=0;i<pathname_cnt;i++)
            cJSON_AddItemToArray(pathnames_o, cJSON_CreateString(pathnames[i]));
    }
    cJSON_add_string(new_result, "rootfs",rootfs);
    cJSON_AddItemToArray(result, new_result);
    return 0;
}

int dkr_api_blob_get(struct dkr_api_data_s * dkr_api_data,const char *image,const char *blobSum, char ** result)
{
    return *((int*)dkr_api_data->test);
}


int dkr_api_get_manifest( struct dkr_api_data_s * dkr_api_data ,const char * accept,const char * image, cJSON ** result)
{
    return *((int*)dkr_api_data->test);
}

int dkr_api_manifest_exists( struct dkr_api_data_s * dkr_api_data ,const char * image,char ** image_sha, char ** tag)
{
    return *((int*)dkr_api_data->test);
}

const char var_ws[]="[{\n\t\t\t\"name\":\t\"repo C7\",\n\t\t\t\"key\":\t\"x86_64\",\n\t\t\t\"scope\":\t\"global\",\n\t\t\t\"path\":\t[\"$CACHE_DIR/base\", \"$CACHE_DIR/updates\"],\n\t\t\t\"strategy\":\t\"push-pull\",\n\t\t\t\"threshold\":\t{\n\t\t\t\t\"path\":\t[\"$CACHE_DIR/base/*.rpm\", \"$CACHE_DIR/updates/*.rpm\"]\n\t\t\t}\n\t\t}, {\n\t\t\t\"name\":\t\"deployctl\",\n\t\t\t\"key\":\t\"x86_64\",\n\t\t\t\"scope\":\t\"global\",\n\t\t\t\"path\":\t\"$CACHE_DIR/deployctl\",\n\t\t\t\"strategy\":\t\"push-pull\",\n\t\t\t\"threshold\":\t{\n\t\t\t\t\"path\":\t\"$CACHE_DIR/deployctl/*.rpm\"\n\t\t\t}\n\t\t}]";


int main(void) {
    
    int res=0;
    const char command[]="registry_push --rootfs=$HOME/test_reg --ISR * /Users/dgoo2308";
    char pwdir[PATH_MAX];
    const char *this_dir=getcwd(pwdir,PATH_MAX);
    cJSON * env_vars=cJSON_CreateObject();
    cJSON * job=cJSON_CreateObject();
    cJSON_AddItemToObject(job, "env_vars", env_vars);
    cJSON_add_string(env_vars, "HOME", getenv("HOME"));
    
    Environment *e=Environment_init(env_vars, environment_type_cJSON);
    Environment_setenv(e, "HOME", getenv("HOME"), 1);
    Environment_setenv(e, "CI_PROJECT_DIR", this_dir, 1);
    Environment_setenv(e, "WORK_SPACES",var_ws, 1);
    Environment_setenv(e, "GIT_CACHE_STRATEGY","push-pull", 1);
    Environment_setenv(e, "GIT_STRATEGY","fetch", 1);
    
    Environment_setenv(e, "CI_COMMIT_REF_SLUG","master", 1);
    Environment_setenv(e, "CI_COMMIT_REF","master", 1);
     Environment_setenv(e, "CACHE_DIR","cache", 1);
    //Environment_setenv(e, "GIT_CACHE_STRATEGY","True", 1);
    
    Environment_setenv(e, "CI_PROJECT_URL","https://gitlab.gioxa.com/dgoo2308/debug_cache",1);
    Environment_setenv(e, "CI_PROJECT_NAME","debug",1 );
    char pwd[1024];
    
    //cJSON* wso=yaml_file_2_cJSON(NULL, "ws.yml");
    //char * wss=cJSON_Print(wso);
    
    Environment_setenv(e, "PWD",getcwd(pwd, 1024), 1);
    
    cJSON * sys_vars=cJSON_CreateObject();
    cJSON_add_string(sys_vars, "ODAGRUN-TOKEN", "123456");
    cJSON_add_string(sys_vars, "ODAGRUN_MASTER-URL", "http://my_masterusrl");
    cJSON_add_string(sys_vars, "ODAGRUN-OC-NAMESPACE", "ODAGRUN");
    cJSON_add_string(sys_vars,"ODAGRUN-IMAGESTRAM-IP","myimagestream");
    cJSON_AddItemToObject(job, "sys_vars", sys_vars);

    vrmdir("%s/%s",this_dir,Environment_getenv(e, "CACHE_DIR"));
    
    struct dkr_api_data_s *ds=calloc(1, sizeof(struct dkr_api_data_s));
    int return_code=200;
    ds->test=&return_code;
    
    setdebug();
    
    int g= parse_git_cache( ds, job,0);
    
    int w=parse_workspaces( ds, job,0);
    
    // add check files
    vmkdir("%s/base",Environment_getenv(e, "CACHE_DIR"));
    vmkdir("%s/deployctl",Environment_getenv(e, "CACHE_DIR"));
    vmkdir("%s/updates",Environment_getenv(e, "CACHE_DIR"));
    const char rpmdata[]="test rpm";
    for (int i=0;i<5;i++)
        write_file_v(rpmdata, 0, "%s/base/t%d.rpm",Environment_getenv(e, "CACHE_DIR"),i);
    for (int i=0;i<6;i++)
        write_file_v(rpmdata, 0, "%s/updates/t%d.rpm",Environment_getenv(e, "CACHE_DIR"),i);
    for (int i=0;i<7;i++)
        write_file_v(rpmdata, 0, "%s/deployctl/t%d.rpm",Environment_getenv(e, "CACHE_DIR"),i);
    
    int remote_objects=6;
    cache_update_remote_objects(job, remote_objects);
    cache_update_initial_filecount(job, NULL, NULL);
    
    for (int i=5;i<8;i++)
        write_file_v(rpmdata, 0, "%s/base/t%d.rpm",Environment_getenv(e, "CACHE_DIR"),i);
    for (int i=7;i<10;i++)
        write_file_v(rpmdata, 0, "%s/deployctl/t%d.rpm",Environment_getenv(e, "CACHE_DIR"),i);
    
    cache_do_push(job, NULL,NULL);
    
    cJSON * layers_push=cJSON_GetObjectItem(job, "layers_push");
    
    print_json(layers_push);
    
    cJSON * result=cJSON_DetachItemFromObject(job, "RESULT");
    debug("\nRESULT:\n");
     print_json(result);
    int fail =0;
    /*
     {
     "image":	"myimagestream/ODAGRUN/is-executer-image:git-cache-2e65e965700566e486277b6935762d6499addda4b3a80159c7d1b316e19b4d63",
     "pathnames":	["/Users/dannygoossen/Library/Developer/Xcode/DerivedData/odagrun-bnqyhzfmllbrsoezncuqiilvnyir/Build/Products/Debug/.git"],
     "rootfs":	"/"
     }
     */
    if (cJSON_GetArraySize(result)!=3 ) fail=1;
    cJSON*result_item=cJSON_GetArrayItem(result, 0);
    const char * image_r=cJSON_get_key(result_item,"image");
    if(strcmp("myimagestream/ODAGRUN/is-executer-image:git-cache-2e65e965700566e486277b6935762d6499addda4b3a80159c7d1b316e19b4d63",image_r)!=0) fail=1;
    cJSON* pathnames_r=cJSON_GetObjectItem(result_item, "pathnames");
    if (cJSON_GetArraySize(pathnames_r)!=1 ) fail=1;
    char * path=NULL;
    asprintf(&path, "%s/.git",Environment_getenv(e, "CI_PROJECT_DIR"));
    const char * path_e=cJSON_GetArrayItem(pathnames_r, 0)->valuestring;
    if (strcmp(path_e,path)!=0)
        fail=1;
    free(path);
    /*
     {
     "image":	"myimagestream/ODAGRUN/is-executer-image:ws-repo-c7-ab8435478408c51d5f549ce510ab4f645494680e1d4b6df3c5a7d3811df4de05",
     "pathnames":	["/Users/dannygoossen/Library/Developer/Xcode/DerivedData/odagrun-bnqyhzfmllbrsoezncuqiilvnyir/Build/Products/Debug/cache/base", "/Users/dannygoossen/Library/Developer/Xcode/DerivedData/odagrun-bnqyhzfmllbrsoezncuqiilvnyir/Build/Products/Debug/cache/updates"],
     "rootfs":	"/"
     }
     */
    result_item=cJSON_GetArrayItem(result, 1);
    pathnames_r=cJSON_GetObjectItem(result_item, "pathnames");
    if (cJSON_GetArraySize(pathnames_r)!=2 ) fail=1;
    /*
     {
     "image":	"myimagestream/ODAGRUN/is-executer-image:ws-deployctl-136035ee82e6d16af009ca305aa5d9c622ed359573b79fa366c061b54b03d1dc",
     "pathnames":	["/Users/dannygoossen/Library/Developer/Xcode/DerivedData/odagrun-bnqyhzfmllbrsoezncuqiilvnyir/Build/Products/Debug/cache/deployctl"],
     "rootfs":	"/"
     }
     */
    result_item=cJSON_GetArrayItem(result, 2);
    pathnames_r=cJSON_GetObjectItem(result_item, "pathnames");
    if (cJSON_GetArraySize(pathnames_r)!=1 ) fail=1;
    
    // and redo cache push, should have no items
    cJSON_Delete(result);
     cache_update_remote_objects(job, 4);
     cache_update_initial_filecount(job, NULL, NULL);
    
    cache_do_push(job, NULL,NULL);
    result=cJSON_DetachItemFromObject(job, "RESULT");
    debug("\nRESULT:\n");
    print_json(result);
    
    
     if ( result)
     {
         fail=1;
   cJSON_Delete(result);
     }
   
  
    vrmdir("%s/%s",this_dir,Environment_getenv(e, "CACHE_DIR"));
    Environment_clear(&e);
    cJSON_Delete(job);
    
    return fail;
}
