//
//  debug_subst_env.c
//  substenv
//
//  Created by Danny Goossen on 7/8/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//


#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../src/subst_env.h"

#include "../src/dyn_buffer.h"
#include "../src/utils.h"



int main (void)
{
	int res=0;
	char * error_msg=NULL;
	
	const char out[]="/Users/dgoo2308/test.md";
	const char in[]="/Users/dgoo2308/test.md.in";
	
	// write test in file
	
	FILE * cin_file=fopen(in, "w");
	IO_Stream * cin_stream=IO_Stream_init(cin_file, io_type_file);
	IO_Stream_fputs( cin_stream,"test\n environment substitution\n var: $hello_key\n");
	IO_Stream_clear(&cin_stream);
	if (cin_file)fclose(cin_file);
	
	FILE * in_file=fopen(in, "r");
	FILE * out_file=fopen(out, "w");
    IO_Stream * in_stream=IO_Stream_init(in_file, io_type_file);
	IO_Stream * out_stream=IO_Stream_init(out_file, io_type_file);
	
	Environment * e=Environment_init(NULL, environment_type_env);
	
	if(e)
	{
	// set env
	e->setenv(e,"hello_key","hallo_value",1);
	}
	
	if (e && in_stream && out_stream)
		res=subst_IO_Stream(in_stream,out_stream,e,&error_msg);
	
	IO_Stream_clear(&in_stream);
	IO_Stream_clear(&out_stream);
	fclose(in_file);
	fclose(out_file);
	
	char * result=read_a_file(out);
	printf("result: %s\n",result);
	remove (out);
	if (e) e->unsetenv(e,"hallo_key");
	if (result) free(result);
	
	Environment_clear(&e);
	
	if (res) printf("ERROR SUBSTITUTION: %s\n",error_msg);
	
	if (error_msg) free(error_msg);
	error_msg=NULL;
	
	// use buffer and chson env
	
	char *data_in=strdup("test\n environment substitution\n var: $hello_key\n<<<");
	
	dynbuffer*in_buff=dynbuffer_init_push(&data_in, 0);
	dynbuffer*out_buff=dynbuffer_init();
	
	in_stream=IO_Stream_init(in_buff, io_type_DynBuffer);
	out_stream=IO_Stream_init(out_buff, io_type_DynBuffer);
	
	e=Environment_init(NULL, environment_type_env);
	
	// set env
	Environment_setenv(e, "hello_key","hallo_value",1);
	
	if (e && in_stream && out_stream)
		res=subst_IO_Stream(in_stream,out_stream,e,&error_msg);
	
	
	IO_Stream_clear(&in_stream);
	IO_Stream_clear(&out_stream);

	dynbuffer_pop(&out_buff, &result, NULL);
	dynbuffer_pop(&in_buff, &data_in, NULL);
	
	Environment_unsetenv(e,"hallo_key");
	
    printf("result_dynbuffer: &&&%s&&&\n",result);
	if (result) free(result);
	
	Environment_clear(&e);
	
	if (res) printf("ERROR SUBSTITUTION: %s\n",error_msg);

	
	
	cJSON * env_vars=cJSON_CreateObject();
	
	in_buff=dynbuffer_init_push(&data_in, 0);
	out_buff=dynbuffer_init();
	
	in_stream=IO_Stream_init(in_buff, io_type_DynBuffer);
	out_stream=IO_Stream_init(out_buff, io_type_DynBuffer);
	
	e=Environment_init(env_vars, environment_type_cJSON);
	
	if (e)
		Environment_setenv(e, "hello_key","hallo_value",1);
	
	
	if (e && in_stream && out_stream) res=subst_IO_Stream(in_stream,out_stream,e,&error_msg);
	
	
	IO_Stream_clear(&in_stream);
	IO_Stream_clear(&out_stream);
	
	dynbuffer_pop(&out_buff, &result, NULL);
	dynbuffer_pop(&in_buff, &data_in, NULL);
	
	if (e) Environment_unsetenv(e,"hallo_key");
	
	printf("result_dynbuffer and cJSON env: &&&%s&&&\n",result);
	if (result) free(result);
	
	Environment_clear(&e);
	
	if (env_vars) cJSON_Delete(env_vars);
	
	if (res) printf("ERROR SUBSTITUTION: %s\n",error_msg);
	
	if (data_in) free(data_in);
	
	
	return 0;
}
