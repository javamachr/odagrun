//
//  docker_io.h
//  oc-runner
//
//  Created by Danny Goossen on 26/1/19.
//  Copyright (c) 2019 Danny Goossen. All rights reserved.
//

#ifndef oc_runner_docker_io_h
#define oc_runner_docker_io_h

#define DOCKER_REGISTRY "docker.io"
//#define DOCKER_REGISTRY_DOMAIN "registry.hub.docker.com"
#define DOCKER_REGISTRY_DOMAIN "index.docker.io"

#endif
