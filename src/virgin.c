/*
 Copyright (c) 2018 by Danny Goossen, Gioxa Ltd.
 
 This file is part of odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/*! @file virgin.c
 *  @brief virgin startup, registering runner, deploy new runner.yml
 *  @author danny@gioxa.com
 *  @date 28/08/18
 *  @copyright (c) 2018 Danny Goossen,Gioxa Ltd.
 */

#include "deployd.h"
#include "oc_api.h"
#include "virgin.h"
#include "simple_http.h"

/*
  - apiVersion: v1
    kind: ImageStream
    metadata:
      name: is-${APPLICATION_NAME}
    spec:
      lookupPolicy:
        local: false
      tags:
        - from:
            kind: DockerImage
            name: registry.gitlab.com/gioxa/odagrun/odagrun/review:latest
          importPolicy:
            scheduled: true
          name: latest
          referencePolicy:
            type: Source
*/

static cJSON* make_ImageStream(const char * namespace, const char * application)
{
	cJSON* ImageStream=cJSON_CreateObject();
	cJSON * metadata=cJSON_CreateObject();
	cJSON * spec=cJSON_CreateObject();
	cJSON_AddStringToObject(ImageStream, "apiVersion", "v1");
	cJSON_AddStringToObject(ImageStream, "kind",       "ImageStream");
	cJSON_AddItemToObject  (ImageStream, "metadata",    metadata);
	char ImageStream_name[256];
	snprintf(ImageStream_name, 256, "is-%s",application);
	cJSON_AddStringToObject(metadata, "name",ImageStream_name);
	cJSON_AddStringToObject(metadata, "namespace",namespace);
	cJSON_AddItemToObject  (ImageStream, "spec",    spec);
	{
		cJSON* tags=cJSON_CreateArray();
		cJSON_AddItemToObject(spec, "tags", tags);
		{
			cJSON*tag=cJSON_CreateObject();
			cJSON_AddItemToArray(tags, tag);
			{
				cJSON* from=cJSON_CreateObject();
				cJSON_AddItemToObject(tag, "from", from);
				{
					cJSON_add_string(from, "kind", "DockerImage");
					const char * im=getenv("ODAGRUN_TEMPLATE_IMAGE");
					if (im && strlen(im)>1)
						cJSON_add_string(from, "name", im);
					else
						cJSON_add_string(from, "name", "gioxa/odagrun");
				}
				cJSON* importPolicy=cJSON_CreateObject();
				cJSON_AddItemToObject(tag, "importPolicy", importPolicy);
				{
					cJSON_AddTrueToObject(importPolicy, "scheduled");
				}
				cJSON_add_string(tag, "name", "latest");
				cJSON* referencePolicy=cJSON_CreateObject();
				cJSON_AddItemToObject(tag, "referencePolicy", referencePolicy);
				{
					cJSON_add_string(referencePolicy, "type", "Source");
				}
			}
		}
	}
	return ImageStream;
}

/*
apiVersion: v1
data:
  runners.yaml: |
    token: 
    ci_url:
    description: ${RUNNER_DESCRIPTION}
kind: ConfigMap
metadata:
  name: runners
  labels:
    DeploymentConfig: odagrun
    application: odagrun
 */

static cJSON* make_configmap_runner(const char * runner, cJSON* meta_labels, const char * namespace, const char * application)
{
	cJSON* config_map=cJSON_CreateObject();
	cJSON * metadata=cJSON_CreateObject();
	cJSON * data=cJSON_CreateObject();
	cJSON_AddStringToObject(config_map, "apiVersion", "v1");
	cJSON_AddStringToObject(config_map, "kind",       "ConfigMap");
	cJSON_AddItemToObject  (config_map, "metadata",    metadata);
	cJSON_AddItemToObject(metadata, "labels", cJSON_Duplicate( meta_labels,1));
	char configmapname[256];
	snprintf(configmapname, 256, "%s-configmap",application);
	cJSON_AddStringToObject(metadata, "name",configmapname);
	cJSON_AddStringToObject(metadata, "namespace",namespace);
	
	cJSON_AddItemToObject  (config_map, "data",  data);
		cJSON_AddStringToObject(data, "runners.yaml", runner);

	return config_map;
}

static cJSON * make_oc_dispatcher_container( const char * application)
{
	cJSON * result=cJSON_CreateObject();
	cJSON_add_string_v(result, "name","cnt-%s", application);
	cJSON_add_string_v(result, "image", "is-%s",application);
	cJSON_add_string(result, "imagePullPolicy", "Always");
	cJSON * command=cJSON_CreateArray();
	cJSON_AddItemToObject(result, "command", command);
	{
		cJSON_add_Array_string(command, "/oc-dispatcher");
	}
	cJSON * env_list=cJSON_CreateArray();
	cJSON_AddItemToObject(result, "env", env_list);
	{
		cJSON * env_item=cJSON_CreateObject();
		cJSON_AddItemToArray(env_list,  env_item);
		{
			cJSON_add_string(env_item, "name", "master_url");
			cJSON_add_string(env_item, "key", "https://openshift.default.svc.cluster.local");
		}
	}
	
	cJSON * probe=cJSON_CreateObject();
	command=  cJSON_CreateStringArray((const char *[]){"/oc-dispatcher","--version",NULL}, 2);
	cJSON * exec=cJSON_CreateObject();
	cJSON_AddItemToObject(exec, "command", command);
	cJSON_AddItemToObject(probe, "exec", exec);
	cJSON_AddNumberToObject(probe, "failureThreshold", 3);
	cJSON_AddNumberToObject(probe, "periodSeconds"  , 20);
	cJSON_AddNumberToObject(probe, "successThreshold", 1);
	cJSON_AddNumberToObject(probe, "timeoutSeconds"  , 1);
	cJSON_AddNumberToObject(probe, "initialDelaySeconds", 120);
	cJSON_AddItemToObject(result, "livenessProbe", probe);
	
	probe=cJSON_CreateObject();
	command=  cJSON_CreateStringArray((const char *[]){"/oc-dispatcher","--ready",NULL}, 2);
	exec=cJSON_CreateObject();
	cJSON_AddItemToObject(exec, "command", command);
	cJSON_AddItemToObject(probe, "exec", exec);
	cJSON_AddNumberToObject(probe, "failureThreshold", 10);
	cJSON_AddNumberToObject(probe, "periodSeconds"   , 15);
	cJSON_AddNumberToObject(probe, "successThreshold", 1);
	cJSON_AddNumberToObject(probe, "timeoutSeconds"  , 1);
	cJSON_AddNumberToObject(probe, "initialDelaySeconds", 60);
	cJSON_AddItemToObject(result, "readinessProbe", probe);
	

	
	
	cJSON * resources=cJSON_CreateObject();
	cJSON_AddItemToObject(result, "resources", resources);
	{
		cJSON * resource_limits=cJSON_CreateObject();
		cJSON_AddItemToObject(resources, "limits",resource_limits);
		cJSON_add_string(resource_limits, "memory", "100Mi");
		
		cJSON * resource_requests=cJSON_CreateObject();
		cJSON_AddItemToObject(resources, "requests",resource_requests);
		cJSON_add_string(resource_requests, "memory", "100Mi");
	}
	cJSON * securityContext=cJSON_CreateObject();
	cJSON_AddItemToObject(result, "securityContext", securityContext);
	{
		cJSON * securityContext_capabilities=cJSON_CreateObject();
		cJSON_AddItemToObject(securityContext, "capabilities", securityContext_capabilities);
		cJSON_AddFalseToObject(securityContext, "privileged");
	}
	cJSON_add_string(result,"terminationMessagePath","/dev/termination-log");
	cJSON_add_string(result,"terminationMessagePolicy","File");
	
	cJSON * volumeMounts=cJSON_CreateArray();
	cJSON_AddItemToObject(result, "volumeMounts", volumeMounts);
	{
		cJSON * volumeMount=cJSON_CreateObject();
		cJSON_AddItemToArray(volumeMounts, volumeMount);
		{
			cJSON_add_string_v(volumeMount,"name","vol-%s-config",application);
			cJSON_add_string(volumeMount, "mountPath",CONFIG_DIR );
		}
	}
	return result;
}


static cJSON * make_odagrun_deployment(const char *namespace, const char * application)
{
	cJSON * result=cJSON_CreateObject();
	cJSON_AddStringToObject(result, "apiVersion", "v1");
	cJSON_AddStringToObject(result, "kind",       "DeploymentConfig");
	cJSON * metadata=cJSON_CreateObject();
	cJSON_AddItemToObject  (result, "metadata",    metadata);
	{
		cJSON*meta_labels=cJSON_CreateObject();
		cJSON_AddItemToObject(metadata, "labels",meta_labels);
		{
			cJSON_add_string(meta_labels, "deploymentConfig", application);
			cJSON_add_string(meta_labels, "application", application);
			cJSON_add_string(meta_labels, "app", "odagrun");
		}
		cJSON_add_string(metadata, "name",application);
		cJSON_add_string(metadata,"namespace",namespace );
	}
	cJSON*spec=cJSON_CreateObject();
	cJSON_AddItemToObject(result, "spec", spec);
	{
		cJSON_safe_addNumber2Obj(spec, "replicas", 1);
		cJSON*selector=cJSON_CreateObject();
		cJSON_AddItemToObject(spec, "selector", selector);
		cJSON_add_string_v(selector, "deploymentConfig","%s" ,application);
		cJSON*strategy=cJSON_CreateObject();
		cJSON_AddItemToObject(spec, "strategy", strategy);
		cJSON_add_string(strategy, "type", "Rolling");
		
		cJSON * rollingParams=cJSON_CreateObject();
		cJSON_AddItemToObject(strategy, "rollingParams", rollingParams);
		{
			cJSON_add_string(rollingParams,"maxUnavailable","90%");
			cJSON_safe_addNumber2Obj(rollingParams,"intervalSeconds", 2);
			cJSON_safe_addNumber2Obj(rollingParams,"maxSurge", 1);
			cJSON_safe_addNumber2Obj(rollingParams,"timeoutSeconds",600);
			cJSON_safe_addNumber2Obj(rollingParams,"updatePeriodSeconds",10);
		}
		cJSON*template=cJSON_CreateObject();
		cJSON_AddItemToObject(spec,"template",template); // TEMPLATE
		{
			cJSON* spec_metadata=cJSON_CreateObject();
			cJSON_AddItemToObject(template, "metadata", spec_metadata);
			{
				cJSON * spec_meta_labels=cJSON_CreateObject();
				cJSON_AddItemToObject(spec_metadata, "labels", spec_meta_labels);
				{
					cJSON_add_string(spec_meta_labels, "deploymentConfig", application);
					char * package_version=slug_it(PACKAGE_STRING, 0);
					cJSON_add_string(spec_meta_labels, "packageString", package_version);
					if (package_version) oc_free(&package_version);
					cJSON_add_string(spec_meta_labels, "application",application);
					cJSON_add_string(spec_meta_labels, "app", "odagrun");
				}
				cJSON_add_string(spec_metadata, "name",application);
			}
			cJSON*template_spec=cJSON_CreateObject();
			cJSON_AddItemToObject(template, "spec", template_spec);
			{
				cJSON*containers=cJSON_CreateArray();
				cJSON_AddItemToObject(template_spec, "containers", containers);
				{
					cJSON * container=make_oc_dispatcher_container( application);
					cJSON_AddItemToArray(containers, container);
					{
						cJSON_add_string(template_spec, "dnsPolicy", "ClusterFirst");
						cJSON_add_string(template_spec, "restartPolicy", "Always");
						cJSON_add_string_v(template_spec, "serviceAccountName", "sa-%s",application);
						cJSON*volumes=cJSON_CreateArray();
						cJSON_AddItemToObject(template_spec, "volumes", volumes);
						{
							cJSON*volume=cJSON_CreateObject();
							cJSON_AddItemToArray(volumes,volume );
							{
								cJSON*volume_config=cJSON_CreateObject();
								cJSON_AddItemToObject(volume, "configMap", volume_config);
								{
									cJSON_safe_addNumber2Obj(volume_config, "defaultMode", 0420);
									cJSON_add_string_v(volume_config, "name", "%s-configmap",application);
								}//volume_ConfigMap
								cJSON_add_string_v(volume, "name", "vol-%s-config",application);
							} //volume
						} //volumes
					} //container
				} //containers
			}//template_spec
		} //template
		cJSON * triggers=cJSON_CreateArray();
		cJSON_AddItemToObject(spec, "triggers", triggers);
		{
			// code for triggers TODO
			cJSON * trigger=cJSON_CreateObject();
			cJSON_AddItemToArray(triggers,trigger);
			{
				cJSON_add_string(trigger, "type", "ImageChange");
				cJSON * imageChangeParams=cJSON_CreateObject();
				cJSON_AddItemToObject(trigger, "imageChangeParams", imageChangeParams);
				{
					cJSON_AddTrueToObject(imageChangeParams, "automatic");
					cJSON_add_string(imageChangeParams, "lastTriggeredImage", "");
					cJSON * containerNames=cJSON_CreateArray();
					cJSON_AddItemToObject(imageChangeParams, "containerNames", containerNames);
					{
						cJSON_add_Array_string_v(containerNames,"cnt-%s", application);
					}
					cJSON * imageChangeParams_from=cJSON_CreateObject();
					cJSON_AddItemToObject(imageChangeParams, "from", imageChangeParams_from);
					{
						cJSON_add_string(imageChangeParams_from, "kind", "ImageStreamTag");
						cJSON_add_string_v(imageChangeParams_from, "name", "is-%s:latest",application);
					}
				}
			}
			trigger=cJSON_CreateObject();
			cJSON_AddItemToArray(triggers,trigger);
			{
				cJSON_add_string(trigger, "type", "ConfigChange");
			}
			
		}//triggers
	} //spec
	return result;
}

void deletepods(struct oc_api_data_s *oc_api_data,const char * application,const char * namespace);

void deletepods(struct oc_api_data_s *oc_api_data,const char * application,const char * namespace)
{
	cJSON* result=NULL;
	cJSON * pods=NULL;
	cJSON * query=cJSON_CreateObject();
	cJSON * deleteoptions=cJSON_CreateObject();
	cJSON_AddNumberToObject(deleteoptions, "gracePeriodSeconds", 60);
	cJSON_add_string_v(query, "labelSelector","deploymentConfig=%s", application);
	cJSON_safe_addBool2Obj(query, "includeUninitialized", 1);
	int pod_res=oc_api(oc_api_data, "GET", query, &pods,"/api/v1/namespaces/%s/pods",namespace);
	
	if (pod_res && pod_res!=404)
	{
		print_api_error_stderr(pod_res);
		if (pods)
		{
			print_json(pods);
		}
	}
	if (pod_res && pods)
	{
		cJSON_Delete(pods);
		pods=NULL;
	}
	
	if (!pod_res && pods)
	{
		cJSON*items=cJSON_GetObjectItem(pods, "items");
		cJSON*item=NULL;
		cJSON_ArrayForEach(item, items)
		{
			const char * podname=NULL;
			cJSON * metadata=cJSON_GetObjectItem(item, "metadata");
			if (metadata)
			{
				cJSON * labels=cJSON_GetObjectItem(metadata, "labels");
				if (labels)
				{
					const char * deploymentconfig=cJSON_get_key(labels, "deploymentConfig");
					if (deploymentconfig && strcmp(deploymentconfig,application)==0)
						podname=cJSON_get_key(metadata, "name");
				}
			}
			if (podname)
			{
				alert("delete pod %s\n",podname);
				oc_api(oc_api_data, "DELETE", deleteoptions, &result,"/api/v1/namespaces/%s/pods/%s",namespace,podname);
			}
		}
	}
	if(deleteoptions) cJSON_Delete(deleteoptions);
	if(query)cJSON_Delete(query);
	if(pods)cJSON_Delete(pods);
	if (result) cJSON_Delete(result);
	result=NULL;
}

void deletereplicationcontrolers(struct oc_api_data_s *oc_api_data,const char * application,const char * namespace);

void deletereplicationcontrolers(struct oc_api_data_s *oc_api_data,const char * application,const char * namespace)
{
	alert("check for replication controlers\n");
	cJSON* result=NULL;
	cJSON * pods=NULL;
	cJSON * query=cJSON_CreateObject();
	cJSON * deleteoptions=cJSON_CreateObject();
	cJSON_AddNumberToObject(deleteoptions, "gracePeriodSeconds", 60);
	cJSON_add_string_v(query, "labelSelector","deploymentConfig=%s", application);
	int pod_res=oc_api(oc_api_data, "GET", query, &pods,"/api/v1/namespaces/%s/replicationcontrollers",namespace);
	
	if (pod_res && pod_res!=404)
	{
		print_api_error_stderr(pod_res);
		if (pods)
		{
			print_json(pods);
		}
	}
	if (pod_res && pods)
	{
		cJSON_Delete(pods);
		pods=NULL;
	}
	
	if (!pod_res && pods)
	{
		cJSON*items=cJSON_GetObjectItem(pods, "items");
		cJSON*item=NULL;
		cJSON_ArrayForEach(item, items)
		{
			const char * podname=NULL;
			cJSON * metadata=cJSON_GetObjectItem(item, "metadata");
			if (metadata)
			{
				cJSON * labels=cJSON_GetObjectItem(metadata, "labels");
				if (labels)
				{
					const char * deploymentconfig=cJSON_get_key(labels, "openshift.io/deployment-config.name");
					if (deploymentconfig && strcmp(deploymentconfig,application)==0)
						podname=cJSON_get_key(metadata, "name");
				}
			}
			if (podname)
			{
				alert("delete replication controler: %s\n",podname);
				oc_api(oc_api_data, "DELETE", deleteoptions, &result,"/api/v1/namespaces/%s/replicationcontrollers/%s",namespace,podname);
			}
		}
	}
	if(deleteoptions) cJSON_Delete(deleteoptions);
	if(query)cJSON_Delete(query);
	if(pods)cJSON_Delete(pods);
	if (result) cJSON_Delete(result);
	result=NULL;
}


void delete_runners(CURL * curl,const char *old_runners_yaml);

void delete_runners(CURL * curl,const char *old_runners_yaml)
{
	alert("unregister runners\n");
	setdebug();
	cJSON * headers=cJSON_CreateObject();
	if (headers)
	{
		cJSON_add_string_v(headers, "Content-Type", "application/json");
	}
	cJSON * runners=yaml_sting_2_cJSON(NULL, old_runners_yaml);
	print_json(runners);
	cJSON*item=NULL;
	int i=0;
	cJSON_ArrayForEach(item, runners)
	{
		i++;
		const char * ci_url=cJSON_get_key(item, "ci_url");
		const char * token=cJSON_get_key(item, "token");
		alert("delete old runner %d: %s on %s\n",i,token,ci_url);
		cJSON * payload=cJSON_CreateObject();
		cJSON_add_string(payload, "token", token);
		char * payload_str=cJSON_PrintUnformatted(payload);
		cJSON_Delete(payload);
		payload=NULL;
		debug("payload for delete: %s\n",payload_str);
		char * del_result=NULL;
		int res=custom_http_v(curl, payload_str, 0, headers, &del_result, NULL, NULL, "DELETE", "%s/api/v4/runners" ,ci_url);
		if (res!=204)
		{
			print_api_error_stderr(res);
			if (del_result)
			{
				error("%s",del_result);
			}
		}
		if (payload_str) oc_free(&payload_str);
		payload_str=NULL;
		if(del_result) oc_free(&del_result);
		del_result=NULL;
	}
	if (headers) cJSON_Delete(headers);

}


int virgin_process(parameter_t * parameters)
{
	
	
	char * token=read_a_file_v( "%s/token",parameters->service_dir);
	char *namespace=read_a_file_v("%s/namespace",parameters->service_dir);
	
	char * master_url=NULL;
	const char * master_url_env=getenv("ODAGRUN_MASTER_URL");
	if (master_url_env)
		master_url=strdup(master_url_env);
	else
		master_url=strdup("https://openshift.default.svc.cluster.local:443");
	
	if (parameters->shell_exec)
	{ // if we run virgin remote, we don't need internal certificates, but default public ones should do
		if (parameters->ca_bundle) free(parameters->ca_bundle);
		parameters->ca_bundle=NULL;
	}
	
	struct oc_api_data_s *oc_api_data=NULL;
	oc_api_init(&oc_api_data, parameters->ca_bundle, master_url, token);
	
	CURL* curl =simple_http_init(parameters->ca_bundle, easy_http_follow_location, easy_http_quiet, easy_http_SSL_VERIFYPEERL, PACKAGE_STRING);
	int res=0;
	alert("start virgin process\n");

	int update_configmap=0;
	
	const char *application=getenv("ODAGRUN_APPLICATION_NAME");
	const char *ci_url=getenv("GITLAB_RUNNER_CI_URL");
	const char *reg_token=getenv("GITLAB_RUNNER_REGISTRATION_TOKEN");
	const char * taglist_raw= getenv("GITLAB_RUNNER_TAGS");
	const char * description=getenv("GITLAB_RUNNER_DESCRIPTION");
	
	
	int replace=env_var_is_true("ODAGRUN_CONFIG_REPLACE");
	int delete=env_var_is_true("ODAGRUN_DELETE_ODAGRUN");
	int untagged=env_var_is_true("GITLAB_RUNNER_UNTAGGED");
	int locked=env_var_is_true("GITLAB_RUNNER_LOCKED");
	
	if (delete)
	{ // verify CONFIRM
		if (!(reg_token && strcmp(reg_token,"CONFIRM")==0))
		{
			alert("delete requested but not confirmed by CONFIRM in token\n");
			alert("delete CANCELLED, exit\n");
			simple_http_clean(&curl);
			alert("DELETED ALL, Sleep 5 min and exit\n");
			int i=300;
			for(i=i;i>0;i--)
			{
				usleep(1000000);
				if (parameters->exitsignal) break;
				if (parameters->hubsignal) break;
			}
			
			alert("done sleeping\n");
			{
				cJSON * result=NULL;
				alert("Delete virgin Pod\n");
				cJSON * deleteoptions=cJSON_CreateObject();
				cJSON_AddNumberToObject(deleteoptions, "gracePeriodSeconds", 5);
				int del_res=oc_api(oc_api_data, "DELETE", deleteoptions, &result,"/api/v1/namespaces/%s/pods/pod-%s-virgin",namespace,application);
				if (deleteoptions) cJSON_Delete(deleteoptions);
				deleteoptions=NULL;
				if (del_res)print_api_error_stderr(del_res);
				if (result) cJSON_Delete(result);
				result=NULL;
			}
			return -1;
		}
	}
	
	cJSON * tag_list=comma_sep_list_2_json_array(taglist_raw);
	print_json(tag_list);
	
	if ((!application||!ci_url||!reg_token||!tag_list) && (!delete))
	{
		alert("Exit, missing parameters\n");
		usleep(30000000);
		return -1;
	}
	
	cJSON*configmap_old=NULL;
	alert("check if old configMap\n");
	res=oc_api(oc_api_data, "GET", NULL,&configmap_old,"/api/v1/namespaces/%s/configmaps/%s-configmap",namespace,application);
	if (res && res!=404)
	{
		alert("failed getting existing config map\n");
		print_api_error_stderr(res);
	}
	if (!res && configmap_old)
	{
		alert("got a previous runner config map\n");
		update_configmap=1;
	}
	if (res==404) res=0;
	
//TODO: implement delete
	char * old_runners_yaml=NULL;
	if (configmap_old)
	{
		cJSON*data=cJSON_GetObjectItem(configmap_old, "data");
		if (data)
			old_runners_yaml=cJSON_get_stringvalue_dup(data, "runners.yaml");
		alert("Old runners config=\n%s\n",old_runners_yaml);
	}
	
	if (delete)
	{
		alert("DELETE resourses\n");
		deletereplicationcontrolers(oc_api_data, application, namespace);
		
		{ // delete deploymentconfigs
			cJSON * result=NULL;
			alert("delete deploymentConfig %s\n",application);
			cJSON * deleteoptions=cJSON_CreateObject();
			cJSON_AddNumberToObject(deleteoptions, "gracePeriodSeconds", 0);
			int res_del=oc_api(oc_api_data, "DELETE", deleteoptions, &result,"/oapi/v1/namespaces/%s/deploymentconfigs/%s",namespace,application);
			if (deleteoptions) cJSON_Delete(deleteoptions);
			deleteoptions=NULL;
			if (res_del)print_api_error_stderr(res_del);
			if (result) cJSON_Delete(result);
			result=NULL;
		}
		deletepods(oc_api_data, application, namespace);
		{
			cJSON * result=NULL;
			alert("Delete config Map\n");
			cJSON * deleteoptions=cJSON_CreateObject();
			cJSON_AddNumberToObject(deleteoptions, "gracePeriodSeconds", 0);
			int del_res=oc_api(oc_api_data, "DELETE", deleteoptions, &result,"/api/v1/namespaces/%s/configmaps/%s-configmap",namespace,application);
			if (deleteoptions) cJSON_Delete(deleteoptions);
			deleteoptions=NULL;
			if (del_res)print_api_error_stderr(del_res);
			if (result) cJSON_Delete(result);
			result=NULL;
		}
		
		if (old_runners_yaml)
			delete_runners(curl,old_runners_yaml);
		
		{ // delete imagestream
			cJSON * result=NULL;
			alert("Delete imagestream\n");
			cJSON * deleteoptions=cJSON_CreateObject();
			cJSON_AddNumberToObject(deleteoptions, "gracePeriodSeconds", 0);
			int del_res=oc_api(oc_api_data, "DELETE", deleteoptions, &result,"/oapi/v1/namespaces/%s/imagestreams/is-%s",namespace,application);
			if (deleteoptions) cJSON_Delete(deleteoptions);
			deleteoptions=NULL;
			if (del_res)print_api_error_stderr(del_res);
			if (result) cJSON_Delete(result);
			result=NULL;
		}

		
		{ // delete ourself while still having an service account
			cJSON * result=NULL;
			alert("Delete virgin Pod\n");
			cJSON * deleteoptions=cJSON_CreateObject();
			cJSON_AddNumberToObject(deleteoptions, "gracePeriodSeconds", 20);
			int del_res=oc_api(oc_api_data, "DELETE", deleteoptions, &result,"/api/v1/namespaces/%s/pods/pod-%s-virgin",namespace,application);
			if (deleteoptions) cJSON_Delete(deleteoptions);
			deleteoptions=NULL;
			if (del_res)print_api_error_stderr(del_res);
			if (result) cJSON_Delete(result);
			result=NULL;
		}
	
	
		alert("DELETED ALL, Sleep 1 min and exit\n");
		int i=60;
		for(i=i;i>0;i--)
		{
			usleep(1000000);
			if (parameters->exitsignal) break;
		}
		
		alert("done sleeping\n");
		
		if(configmap_old) cJSON_Delete(configmap_old);
		if (old_runners_yaml) oc_free(&old_runners_yaml);
		old_runners_yaml=NULL;
		simple_http_clean(&curl);
		usleep(30000000);
		return(0);
	}
	
	
	if (configmap_old)
	{
		if (replace && old_runners_yaml)
		{
			alert("\nREPLACE\nreplacing old configMap\n");
			// unregister the runners
			delete_runners(curl,old_runners_yaml);
			if (old_runners_yaml) oc_free(&old_runners_yaml);
			old_runners_yaml=NULL;
		}
		cJSON_Delete(configmap_old);
		configmap_old=NULL;
	}
	
	
	cJSON * data_register_runner=cJSON_CreateObject();
	cJSON_add_string(data_register_runner,"token" ,reg_token );
	
	cJSON_AddItemToObject(data_register_runner, "tag_list",tag_list);
	tag_list=NULL;
	cJSON_add_string(data_register_runner, "description", description);
	cJSON_safe_addBool2Obj(data_register_runner, "run_untagged", untagged);
	cJSON_safe_addBool2Obj(data_register_runner, "locked", locked);
	char *data=cJSON_PrintUnformatted(data_register_runner);
	debug("\nregister data:%s\n",data);
	cJSON_Delete(data_register_runner);
	cJSON* h_out=cJSON_CreateObject();
	cJSON_add_string(h_out, "Content-Type", "application/json");
	
	char * data_return=NULL;
	int count=0;
	do
	{
		if (count){
			print_api_error_stderr(res);
			usleep(2000000LL * count*count);
		}
		count++;
		alert("\ntry to register new runner %d\n",count);
		res=custom_http_v(curl, data, 0, h_out, &data_return, NULL, NULL, "POST","%s/api/v4/runners/" , ci_url);
		cJSON *result_reg=NULL;
		if (data_return)
			result_reg=cJSON_Parse(data_return);
		if (res==201)
		{
			if(result_reg)
			{
				token=cJSON_get_stringvalue_dup(result_reg, "token");
				res=0;
			}
		}
		else if (result_reg)
		{
			print_json(result_reg);
		}
		
		if(result_reg) cJSON_Delete(result_reg);
			result_reg=NULL;
		if (data_return) oc_free(&data_return);
			data_return=NULL;
	} while (((res>=500 && res<600) || res<0) && count<5);
	usleep(100000);
	cJSON * configmap_runner=NULL;
	 cJSON *result=NULL;
	cJSON * meta_labels=NULL;
	if(!res)
	{
		meta_labels=cJSON_CreateObject();
		cJSON_add_string(meta_labels, "application", application);
		cJSON_add_string(meta_labels, "DeploymentConfig", application);
		cJSON_add_string_v(meta_labels, "createdBy","dc-%s-virgin" ,application);
		cJSON_add_string(meta_labels, "app", "odagrun");
	}
	if (!res)
	{ // got token, so prepare the new config map
		
		dynbuffer *d_buff=dynbuffer_init();
		if (old_runners_yaml)
		{
			dynbuffer_write(old_runners_yaml, d_buff);
			dynbuffer_write("\n---\n\n", d_buff);
		}
		dynbuffer_write_v(d_buff,"ci_url: %s\n",ci_url);
		dynbuffer_write_v(d_buff,"token: %s\n",token);
		dynbuffer_write_v(d_buff,"description: %s\n",description);
		dynbuffer_write_v(d_buff,"application: %s\n",application);
		
		//process
		char * runners_yaml=NULL;
		dynbuffer_pop(&d_buff, &runners_yaml, NULL);
		
		if (runners_yaml)
		{
			configmap_runner=make_configmap_runner(runners_yaml,meta_labels, namespace, application);
			oc_free(&runners_yaml);
			runners_yaml=NULL;
		}
	}
	else
	{
		error(" registring new runner\n");
		usleep(50000000);
	}
	
	
	
	if(configmap_runner)
	{
		
		if (update_configmap)
		{
			alert("going to update the new runners configMap\n");
			res=oc_api(oc_api_data, "PUT", configmap_runner, &result,"/api/v1/namespaces/%s/configmaps/%s-configmap",namespace,application);
		}
		else
		{
			alert("going to create the new runners configMap\n");
			res=oc_api(oc_api_data, "POST", configmap_runner, &result,"/api/v1/namespaces/%s/configmaps",namespace);
		}
		if (res)
		{
			alert("failed writing runner config map\n");
			print_api_error_stderr(res);
			if(result)
			{
				print_json(result);
			}
		}
		else
			alert("SUCCES:  pushed odagrun configMap\n");
		if (result) cJSON_Delete(result);
		result=NULL;
		cJSON_Delete(configmap_runner);
		configmap_runner=NULL;
	}
	else res=-1;
	
	if (old_runners_yaml)
	{
		oc_free(&old_runners_yaml);
		old_runners_yaml=NULL;
	}
	
	cJSON * oc_dispatcher_deployment_config=NULL;
	
	//imageStream
	if (!res)
	{
		int was_debug=getdebug();
		setdebug();
		int have_imagestream=0;
			cJSON * result=NULL;
			alert("check for ImageStream\n");
		
			int head_res=oc_api(oc_api_data, "GET", NULL, &result,"/oapi/v1/namespaces/%s/imagestreams/is-%s",namespace,application);
			if (head_res==404)
			{
				have_imagestream=0;
			}
			else if (head_res==0)
			{
				have_imagestream=1;
			}
			else
			{
				print_api_error_stderr(head_res);
				print_json(result);
			}
			if (result) cJSON_Delete(result);
			result=NULL;
		if (have_imagestream)
		{
			// delete imagestream
			cJSON * result=NULL;
			alert("Delete imagestream\n");
			cJSON * deleteoptions=cJSON_CreateObject();
			cJSON_AddNumberToObject(deleteoptions, "gracePeriodSeconds", 0);
			int del_res=oc_api(oc_api_data, "DELETE", deleteoptions, &result,"/oapi/v1/namespaces/%s/imagestreams/is-%s",namespace,application);
			if (deleteoptions) cJSON_Delete(deleteoptions);
			deleteoptions=NULL;
			if (del_res)print_api_error_stderr(del_res);
			if (result) cJSON_Delete(result);
			result=NULL;
			have_imagestream=0;
		}
		if (!have_imagestream)
		{
			alert("creating ImageStream\n");
			cJSON* imagestream_obj=make_ImageStream(namespace, application);
			if (imagestream_obj)
			{
				res=oc_api(oc_api_data, "POST", imagestream_obj, &result,"/oapi/v1/namespaces/%s/imagestreams",namespace);
				if (res==409)
				{
					alert("is-%s exists\n",application);
					res=0;
				}
				else if(res)
				{
					print_api_error_stderr(res);
					print_json(result);
				}
				else
				{
					alert("is-%s created\n",application);
				}
				if (result) cJSON_Delete(result);
				result=NULL;			
				if (imagestream_obj)cJSON_Delete(imagestream_obj);
				imagestream_obj=NULL;
			}
		}
		if (!was_debug) clrdebug();
	}
	
	
	if (!res)
	{
		cJSON*old_deployment=NULL;
		if (result) cJSON_Delete(result);
		result=NULL;
		alert("get old Deployment config\n");
		int get_res=oc_api(oc_api_data, "GET", NULL, &old_deployment,"/oapi/v1/namespaces/%s/deploymentconfigs/%s",namespace,application);
		if (get_res && get_res!=404)
		{
			print_api_error_stderr(get_res);
			if (old_deployment)
			{
				print_json(old_deployment);
			}
		}
		if (get_res && old_deployment)
		{
			cJSON_Delete(old_deployment);
			old_deployment=NULL;
		}
		if (!get_res && old_deployment)
		{
			// create a restart, just delete the pods, replication controller will create new ones!
			deletepods(oc_api_data, application, namespace);
		}
		if (!old_deployment)
		{
				alert("Creating new deployment config object\n");
				oc_dispatcher_deployment_config=make_odagrun_deployment(namespace, application);
		}
		if (oc_dispatcher_deployment_config)
		{
			alert("\nPUT new Deployment config\n");
			print_json(oc_dispatcher_deployment_config);
			res=oc_api(oc_api_data, "POST", oc_dispatcher_deployment_config, &result,"/oapi/v1/namespaces/%s/deploymentconfigs/",namespace);
		
			if (res)
			{
				print_api_error_stderr(get_res);
				if (result)
				{
					print_json(result);
				}
			}
		}
		if (result) cJSON_Delete(result);
		result=NULL;
		if (old_deployment)
		{
			cJSON_Delete(old_deployment);
			old_deployment=NULL;
		}
	}
	
	if(oc_dispatcher_deployment_config)
	{
		cJSON_Delete(oc_dispatcher_deployment_config);
		oc_dispatcher_deployment_config=NULL;
	}
	
	int i=0;
	
	if (res)
	{
		alert("error, Sleep 5 minutes or till we called to exit\n");
		i=300;
	}
	else
	{
		alert("success, Sleep 30 seconds and exit\n");
		i=30;
	}
	for(i=i;i>0;i--)
	{
		usleep(1000000);
		if (parameters->exitsignal) break;
		if (parameters->hubsignal) break;
	}
	
	alert("done sleeping\n");
	{
		if (result) cJSON_Delete(result);
		result=NULL;
		alert("Delete virgin Pod\n");
		cJSON * deleteoptions=cJSON_CreateObject();
		cJSON_AddNumberToObject(deleteoptions, "gracePeriodSeconds", 5);
		int del_res=oc_api(oc_api_data, "DELETE", deleteoptions, &result,"/api/v1/namespaces/%s/pods/pod-%s-virgin",namespace,application);
		if (deleteoptions) cJSON_Delete(deleteoptions);
		deleteoptions=NULL;
		if (del_res)print_api_error_stderr(del_res);
		if (result) cJSON_Delete(result);
		result=NULL;
	}
	if (res) return res;
	
	return res;
}
