/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.

 This file is part of the odagrun

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.


 Modified code from:

 http://git.savannah.gnu.org/cgit/gettext.git/tree/gettext-runtime/src/envsubst.c

 For use with IO_Stream and Environment, and error handling.

 License Source: GPL3

 Substitution of environment variables in shell format strings.
 Copyright (C) 2003-2007, 2012, 2015-2018 Free Software Foundation, Inc.
 Written by Bruno Haible <bruno@clisp.org>, 2003.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.


 */

/**
 * \file subst_env.h
 * \brief headers for Substitude variables in IO_Stream from Environment
 * \author Created by Danny Goossen on  8/8/18.
 * \copyright 2018, Danny Goossen. MIT License
 * \note modified Source from: http://git.savannah.gnu.org/cgit/gettext.git/tree/gettext-runtime/src/envsubst.c
 */


#ifndef __odagrun__subst_env__
#define __odagrun__subst_env__

#include "common.h"
#include <stdio.h>
#include "IO_Stream.h"
#include "Environment.h"

/**
 * \brief Copies in_stream to out_stream while performing substitutions from Environment
 *  \code //
 // example usage subst file
 //-------------------------
 FILE * in_file=fopen("filetosubstitude.txt", "r");
 FILE * out_file=fopen("result.txt", "w");

 IO_Stream * in_stream=IO_Stream_init(in_file, io_type_file);
 IO_Stream * out_stream=IO_Stream_init(out_file, io_type_file);

 Environment * e=Environment_init(NULL, environment_type_env);

 if(e) e->setenv(e,"hello_key","hallo_value",1);

 // and substitude
 if (e && in_stream && out_stream)
	res=subst_IO_Stream(in_stream,out_stream,e,&error_msg);

 IO_Stream_clear(&in_stream);
 IO_Stream_clear(&out_stream);
 fclose(in_file);
 fclose(out_file);

 //------------------------------------------------------
 // example using dynbuffer and cJSON Environment object
 //------------------------------------------------------
 char *data_in=strdup("test\n environment substitution\n var: $hello_key\n<<<");
 cJSON * env_vars=cJSON_CreateObject();

 in_buff=dynbuffer_init_push(&data_in, 0);
 out_buff=dynbuffer_init();

 in_stream=IO_Stream_init(in_buff, io_type_DynBuffer);
 out_stream=IO_Stream_init(out_buff, io_type_DynBuffer);

 e=Environment_init(env_vars, environment_type_cJSON);

 // set an environment variable
 if (e) e->setenv(e,"hello_key","hallo_value",1);

 // and substitude the variables in our buffer
 if (e && in_stream && out_stream)
 	res=subst_IO_Stream(in_stream,out_stream,e,&error_msg);

 IO_Stream_clear(&in_stream);
 IO_Stream_clear(&out_stream);

 // pop data from the dynbuffer
 dynbuffer_pop(&out_buff, &result, NULL);
 dynbuffer_pop(&in_buff, &data_in, NULL);

 printf("result_dynbuffer and cJSON env: &&&%s&&&\n",result);

 Environment_clear(&e);

 //Cleanup data
 if (result) oc_free(&result);
 if (env_vars) cJSON_Delete(env_vars);
 if (res) printf("ERROR SUBSTITUTION: %s\n",error_msg);
 if (data_in) oc_free(&data_in);

 */
int subst_IO_Stream(IO_Stream * in_stream,IO_Stream * out_stream,Environment * e,char**errormsg);

#endif /* defined(__odagrun__subst_env__) */
