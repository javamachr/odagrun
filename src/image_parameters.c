/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.
 
 This file is part of the odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

/**
 * \file image_parameters.c
 * \brief parameter handling image for registry/Imagestream etc
 * \author Created by Danny Goossen on  9/8/18.
 * \copyright 2018, Danny Goossen. MIT License
 */

#include "deployd.h"
#include <getopt.h>
#include "image_parameters.h"
#include "docker_io.h"

#pragma GCC diagnostic ignored "-Wmissing-field-initializers"

/**
 \brief internal
 \param env_vars environment variables defined per ci-job in struct cJSON
 \param parameter docker registry image parameters
 */
static void set_missing_parms_push(cJSON * env_vars,dkr_parameter_t *parameter);

/**
 \brief assemble image nick name
 \param nick_registry name of the nick registry
 \param parameters the image parameters
*/
void form_image_nick(const char * nick_registry,dkr_parameter_t * parameters);

void clear_dkr_parameters(dkr_parameter_t **parameter)
{
	if (parameter && *parameter)
	{
		oc_free(&(*parameter)->info_command);
		oc_free(&(*parameter)->rootfs);
		oc_free(&(*parameter)->image_name);
		oc_free(&(*parameter)->image_description);
		oc_free(&(*parameter)->image_long_description_filename);
		oc_free(&(*parameter)->reference);
		oc_free(&(*parameter)->image_dir);
		oc_free(&(*parameter)->image);
		oc_free(&(*parameter)->registry_name);
		oc_free(&(*parameter)->namespace);
		oc_free(&(*parameter)->tag);
		oc_free(&(*parameter)->creds);
		oc_free(&(*parameter)->image_nick);
		oc_free(&(*parameter)->archive);
		oc_free(&(*parameter)->layer_blob_digest);
		oc_free(&(*parameter)->layer_content_digest);
		oc_free(&(*parameter)->docker_config_digest);
		free(*parameter);
		(*parameter)=NULL;
	}
	return;
}

//TODO: have a parameter wich options are allowed
int process_options(size_t argc, char *const*argv, dkr_parameter_t *parameter)
{
	
	struct option long_options[] = {
		{ "verbose",       0, NULL, 'v' },
		{ "quiet",         0, NULL, 'q' },
		{ "u2g",           0, NULL, 'u' },
		{ "rootfs",        1, NULL, 'p' },
		{ "layer",         1, NULL, 'l' },
		{ "reference",     1, NULL, 'r' },
		{ "tag",           1, NULL, 't' },
		{ "image_dir",     1, NULL, 'i' },
		{ "image-dir",     1, NULL, 'i' },
		{ "name",          1, NULL, 'n' },
		{ "allow_fail",    0, NULL, 'f' },
		{ "allow-fail",    0, NULL, 'f' },
		{ "allow-failure", 0, NULL, 'f' },
		{ "allow_failure", 0, NULL, 'f' },
		{ NULL,         0, NULL, 0 } };
	
	int           which;
	optind=0;  // reset if called again, needed for fullcheck as we call twice (server and main)
	// ref : http://man7.org/linux/man-pages/man3/getopt.3.html
	if (argc>1)
	{
		/* for each option found */
		while ((which = getopt_long((int)argc, argv, "+", long_options, NULL)) > 0) {
			
			/* depending on which option we got */
			switch (which) {
					/* --verbose    : enter verbose mode for debugging */
				case 'v':
				{
					if (parameter->quiet)
					{
						error("Invalid option, choose quiet or verbose\n");
						return -1;
					}
					else
					{
						parameter->verbose = 1;
					}
				}
					break;
				case 'q':
				{
					if (parameter->verbose)
					{
						error("Invalid option, choose quiet or verbose\n");
						return -1;
					}
					else
						parameter->quiet = 1;
				}
					break;
					
					/* --rootfs         : path to rootfs to transform */
				case 'u': {
					parameter->u2g=1;
				}
					break;
				case 'p': {
					parameter->rootfs=get_opt_value(optarg,"rootfs",NULL);
				}
					break;
				case 'l': {
			
					parameter->archive=get_opt_value(optarg,"layer.tar.gz",NULL);
				}
					break;
				case 'r': {
					parameter->reference=get_opt_value(optarg,NULL,NULL);
				}
					break;
				case 'i': {
				
					parameter->image_dir=get_opt_value(optarg,"image",NULL);
				}
					break;
				case 'n': {
					parameter->image_name=get_opt_value(optarg,NULL,NULL);
				}
			 case 't': {
					parameter->tag=get_opt_value(optarg,NULL,NULL);
			 }break;
				case 'f': {
					parameter->allow_fail=1;
				}
					break;
				case 'V':
				{
					error("%s\n",GITVERSION);
					exit(0);
				}
					/* otherwise    : display usage information */
				default:
					;
					break;
			}
		}
	}
	return optind;
}

int prep_registry(const cJSON * job, dkr_parameter_t * parameters,struct dkr_api_data_s ** api_data,struct oc_api_data_s **oc_api_data)
{
	
	const cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	const char * project_dir=cJSON_get_key(env_vars,"CI_PROJECT_DIR");
	
	debug("working in %s\n",project_dir);
	
	int exit_code=0;
	
	if (parameters->ISR)
	{
		set_ISR_registry(job, parameters, api_data,oc_api_data);
	}
	else if (parameters->GLR)
	{
		if (!cJSON_get_key(env_vars, "CI_REGISTRY_IMAGE")) exit_code=-1;
		if (!exit_code)
			set_GLR_registry(job, parameters, api_data);
	}
	else exit_code=set_registry(job, parameters, api_data, oc_api_data);
	
	
	return( exit_code);
}

int set_registry(const cJSON * job, dkr_parameter_t * parameters,struct dkr_api_data_s ** api_data,struct oc_api_data_s **oc_api_data)
{
	const cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	const cJSON * sys_vars=cJSON_GetObjectItem(job ,"sys_vars");
	const char* ImageStream_domain=cJSON_get_key(sys_vars,"ODAGRUN-IMAGESTRAM-IP");
	int IMAGESTRAM_Insecure=cJSON_safe_IsTrue(sys_vars,"ODAGRUN-IMAGESTRAM-Insecure");
	const char * ODAGRUN_NAMESPACE=cJSON_get_key(sys_vars,"ODAGRUN-OC-NAMESPACE");
	const char * token= cJSON_get_key(sys_vars,"ODAGRUN-TOKEN");
	const char * master_url=cJSON_get_key(sys_vars,"OC-MASTER-URL");
	const char * docker_creds=cJSON_get_key(env_vars, "DOCKER_CREDENTIALS");
	const char * quay_creds=cJSON_get_key(env_vars, "QUAY_CREDENTIALS");
	const char * quay_oauth_token=cJSON_get_key(env_vars, "QUAY_OAUTH_TOKEN");
	
	if (parameters->image)
	{
		debug("set registry\n");
		if (!*api_data) dkr_api_init(api_data, DEFAULT_CA_BUNDLE);
		parameters->image_nick=parameters->image;
		parameters->image=NULL;
		debug("processing image_nick: %s \n",parameters->image_nick);
		parameters->image=process_image_nick(&parameters->image_nick, env_vars, ImageStream_domain, ODAGRUN_NAMESPACE);
		
		//TODO
		if (parameters->image)
		{
			debug("chop image %s \n",parameters->image);
			chop_image(parameters->image,&parameters->registry_name,&parameters->namespace,&parameters->reference);
			// need something, token gitlab / token imagestream
			if (strcmp(parameters->registry_name,ImageStream_domain)==0)
			{
				if (parameters->creds)
					dkr_api_add_registry(*api_data, parameters->registry_name,parameters->namespace,NULL,NULL, parameters->creds,IMAGESTRAM_Insecure);
				else
					dkr_api_add_registry(*api_data, parameters->registry_name, parameters->namespace,"",token, NULL,IMAGESTRAM_Insecure);
				parameters->ISR=1;
			}
			else if (cJSON_get_key(env_vars, "CI_REGISTRY") && strcmp(parameters->registry_name,cJSON_get_key(env_vars, "CI_REGISTRY"))==0)
			{
				if (parameters->creds)
					dkr_api_add_registry(*api_data, parameters->registry_name,parameters->namespace ,NULL,NULL, parameters->creds,0);
				else
					dkr_api_add_registry(*api_data, parameters->registry_name,parameters->namespace ,cJSON_get_key(env_vars,"CI_REGISTRY_USER" ), cJSON_get_key(env_vars,"CI_REGISTRY_PASSWORD" ), NULL,0);
				parameters->GLR=1;
			}
			else
			{
				if (parameters->creds)
					dkr_api_add_registry(*api_data, parameters->registry_name, parameters->namespace, NULL,NULL, parameters->creds,0);
				else if (strcmp(parameters->registry_name,DOCKER_REGISTRY)==0 && docker_creds)
				{
					parameters->creds=strdup(docker_creds);
					dkr_api_add_registry(*api_data, parameters->registry_name, parameters->namespace, NULL,NULL, parameters->creds,0);
				}
				else if (strcmp(parameters->registry_name,QUAY_REGISTRY)==0 && (quay_creds||quay_oauth_token))
				{
					if (quay_creds) // priority for quay creds iso quat oath token
					   dkr_api_add_registry(*api_data, parameters->registry_name, parameters->namespace, NULL,NULL ,quay_creds,0);
					else
						dkr_api_add_registry(*api_data, parameters->registry_name, parameters->namespace, "$oauthtoken",quay_oauth_token ,NULL,0);
				}
				else
					dkr_api_add_registry(*api_data, parameters->registry_name, parameters->namespace,NULL,NULL, NULL,0);			}
			
			if (strcmp(parameters->registry_name,ImageStream_domain)==0 && oc_api_data && !*oc_api_data)
			{
				oc_api_init(oc_api_data, DEFAULT_CA_BUNDLE, master_url, token);
			}
			
			return 0;
		}
		else
		{
			debug("no image after processing nick\n");
			return -1;
		}
	}
	else
	{
		debug("no image to work with\n");
		return -1;
	}
}

int set_registry_by_image(const cJSON * job, const char * image,struct dkr_api_data_s * api_data)
{
	const cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	const cJSON * sys_vars=cJSON_GetObjectItem(job ,"sys_vars");
	const char* ImageStream_domain=cJSON_get_key(sys_vars,"ODAGRUN-IMAGESTRAM-IP");
	int IMAGESTRAM_Insecure=cJSON_safe_IsTrue(sys_vars,"ODAGRUN-IMAGESTRAM-Insecure");
	const char * ODAGRUN_NAMESPACE=cJSON_get_key(sys_vars,"ODAGRUN-OC-NAMESPACE");
	const char * token= cJSON_get_key(sys_vars,"ODAGRUN-TOKEN");
	const char * docker_creds=cJSON_get_key(env_vars, "DOCKER_CREDENTIALS");
	const char * quay_creds=cJSON_get_key(env_vars, "QUAY_CREDENTIALS");
	const char * quay_oauth_token=cJSON_get_key(env_vars, "QUAY_OAUTH_TOKEN");
	dkr_parameter_t * parameters=calloc(1, sizeof(dkr_parameter_t));
	parameters->image=strdup(image);
	if (parameters->image)
	{
		debug("set registry\n");
		parameters->image_nick=parameters->image;
		parameters->image=NULL;
		debug("processing image_nick: %s \n",parameters->image_nick);
		parameters->image=process_image_nick(&parameters->image_nick, env_vars, ImageStream_domain, ODAGRUN_NAMESPACE);
		
		//TODO
		if (parameters->image)
		{
			debug("chop image %s \n",parameters->image);
			chop_image(parameters->image,&parameters->registry_name,&parameters->namespace,&parameters->reference);
			// need something, token gitlab / token imagestream
			if (strcmp(parameters->registry_name,ImageStream_domain)==0)
			{
				if (parameters->creds)
					dkr_api_add_registry(api_data, parameters->registry_name,parameters->namespace,NULL,NULL, parameters->creds,IMAGESTRAM_Insecure);
				else
					dkr_api_add_registry(api_data, parameters->registry_name, parameters->namespace,"",token, NULL,IMAGESTRAM_Insecure);
				parameters->ISR=1;
			}
			else if (cJSON_get_key(env_vars, "CI_REGISTRY") && strcmp(parameters->registry_name,cJSON_get_key(env_vars, "CI_REGISTRY"))==0)
			{
				if (parameters->creds)
					dkr_api_add_registry(api_data, parameters->registry_name,parameters->namespace ,NULL,NULL, parameters->creds,0);
				else
					dkr_api_add_registry(api_data, parameters->registry_name,parameters->namespace ,cJSON_get_key(env_vars,"CI_REGISTRY_USER" ), cJSON_get_key(env_vars,"CI_REGISTRY_PASSWORD" ), NULL,0);
				parameters->GLR=1;
			}
			else
			{
				if (parameters->creds)
					dkr_api_add_registry(api_data, parameters->registry_name, parameters->namespace, NULL,NULL, parameters->creds,0);
				else if (strcmp(parameters->registry_name,DOCKER_REGISTRY)==0 && docker_creds)
					dkr_api_add_registry(api_data, parameters->registry_name, parameters->namespace, NULL,NULL, docker_creds,0);
				else if (strcmp(parameters->registry_name,QUAY_REGISTRY)==0 && (quay_creds||quay_oauth_token))
				{
					if (quay_creds) // priority for quay creds iso quat oath token
						dkr_api_add_registry(api_data, parameters->registry_name, parameters->namespace, NULL,NULL ,quay_creds,0);
					else
						dkr_api_add_registry(api_data, parameters->registry_name, parameters->namespace, "$oauthtoken",quay_oauth_token ,NULL,0);
				}
				else
					dkr_api_add_registry(api_data, parameters->registry_name, parameters->namespace,NULL,NULL, NULL,0);
			}
			
			clear_dkr_parameters(&parameters);
			return 0;
		}
		else
		{
			clear_dkr_parameters(&parameters);
			debug("no image after processing nick\n");
			return -1;
		}
	}
	else
	{
		clear_dkr_parameters(&parameters);
		debug("no image to work with\n");
		return -1;
	}
}



void set_missing_parms_push(cJSON * env_vars,dkr_parameter_t *parameter)
{
	// we should check env
	
	if(parameter->archive==NULL) {
		parameter->archive=create_stringf("layer.tar.gz");
	}
	if(parameter->image_dir==NULL) {
		parameter->image_dir=create_stringf("%s/image",cJSON_get_key(env_vars, "CI_PROJECT_DIR"));
	}
	if (! parameter->verbose && !parameter->quiet) parameter->verbose = VERBOSE_YN;
	return;
}




int prep_GLR_registry(const cJSON * job, dkr_parameter_t * parameters,struct dkr_api_data_s ** api_data,size_t argc,char*const*argv)
{
	if ((optind=process_options(argc, argv,parameters)) < 0)
	{
		error(" *** ERRORin command line options, exit \n");
		return (-1);
	}
	if (argc > 1 && optind <= (int)argc) {
		alert("ignoring non option arguments\n");
	}
	set_missing_parms_push(	cJSON_GetObjectItem(job ,"env_vars"),parameters);
	const cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	if (!cJSON_get_key(env_vars,"CI_REGISTRY_IMAGE")) return -1;
	set_GLR_registry(job, parameters, api_data);
	return 0;
}

void form_image_nick(const char * nick_registry,dkr_parameter_t * parameters)
{
	parameters->image_nick=create_stringf("%s",nick_registry );
	
	if (parameters->image_name)
	{
		astrcatf(&parameters->image_nick, "/%s",parameters->image_name);
	}
	if(parameters->reference)
	{
		astrcatf(&parameters->image_nick, ":%s",parameters->reference);
	}
	oc_free(&parameters->image);
}

void set_GLR_registry(const cJSON * job, dkr_parameter_t * parameters,struct dkr_api_data_s ** api_data)
{
	const cJSON * sys_vars=cJSON_GetObjectItem(job ,"sys_vars");
	const cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	const char* ImageStream_domain=cJSON_get_key(sys_vars,"ODAGRUN-IMAGESTRAM-IP");
	const char * ODAGRUN_NAMESPACE=cJSON_get_key(sys_vars,"ODAGRUN-OC-NAMESPACE");
	
	//assemble image Nick Name
	form_image_nick("Gitlab", parameters);
	// process image Nick Name
	parameters->image=process_image_nick(&parameters->image_nick, env_vars, ImageStream_domain,ODAGRUN_NAMESPACE);
	// chop image
	chop_image(parameters->image,&parameters->registry_name,&parameters->namespace,&parameters->reference);
	
	// prepare api
	
	if (!(*api_data)) dkr_api_init(api_data, DEFAULT_CA_BUNDLE);
	
	if (parameters->creds)
		dkr_api_add_registry(*api_data, parameters->registry_name,parameters->namespace ,NULL,NULL, parameters->creds,0);
	else
		dkr_api_add_registry(*api_data, parameters->registry_name,parameters->namespace ,cJSON_get_key(env_vars,"CI_REGISTRY_USER" ), cJSON_get_key(env_vars,"CI_REGISTRY_PASSWORD" ), NULL,0);
	
	alert("added registry %s to database\n",parameters->registry_name);
	return;
}

void set_ISR_registry(const cJSON * job, dkr_parameter_t * parameters,struct dkr_api_data_s ** api_data,struct oc_api_data_s **oc_api_data)
{
	const cJSON * sys_vars=cJSON_GetObjectItem(job ,"sys_vars");
	const cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	const char* ImageStream_domain=cJSON_get_key(sys_vars,"ODAGRUN-IMAGESTRAM-IP");
	int IMAGESTRAM_Insecure=cJSON_safe_IsTrue(sys_vars,"ODAGRUN-IMAGESTRAM-Insecure");
	const char * ODAGRUN_NAMESPACE=cJSON_get_key(sys_vars,"ODAGRUN-OC-NAMESPACE");
	const char * token= cJSON_get_key(sys_vars,"ODAGRUN-TOKEN");
	const char * master_url= cJSON_get_key(sys_vars,"OC-MASTER-URL");
	debug("set ImageStream registry\n");
	
	form_image_nick("ImageStream", parameters);
	
	if (!*api_data) dkr_api_init(api_data, DEFAULT_CA_BUNDLE);
	
	parameters->image=process_image_nick(&parameters->image_nick, env_vars, ImageStream_domain,ODAGRUN_NAMESPACE);
	chop_image(parameters->image,&parameters->registry_name,&parameters->namespace,&parameters->reference);
	
	if (parameters->creds)
		dkr_api_add_registry(*api_data, parameters->registry_name,parameters->namespace,NULL,NULL, parameters->creds,IMAGESTRAM_Insecure);
	else
		dkr_api_add_registry(*api_data, parameters->registry_name, parameters->namespace,"",token, NULL,IMAGESTRAM_Insecure);
	alert("added registry %s to database\n",parameters->registry_name);
	
	
	
	if (oc_api_data && !*oc_api_data) oc_api_init(oc_api_data, DEFAULT_CA_BUNDLE, master_url, token);
	
	return;
}
