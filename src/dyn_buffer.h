/*
 
 Created by Danny Goossen, Gioxa Ltd on 8/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

/** \file dyn_buffer.h
 *  \brief header for dynbuf
 *  \author Created by Danny Goossen on 23/7/18.
 *  \copyright Copyright (c) 2018 Danny Goossen. All rights reserved.
 */


#ifndef __deployctl__dyn_buffer__
#define __deployctl__dyn_buffer__

#include "common.h"


/**
 * \brief dynbuffer type
 */
typedef void dynbuffer;


/**
 * \brief append dynbuffer with printf style arguments
 * \return number of characters added, -1 on faulure
 */
size_t dynbuffer_write_v( dynbuffer *userp,const char*data,...);


/**
 * \brief Callback for curl using dynbuffer
 * \return Number of bytes sent
 */
size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb,dynbuffer *userp);

/**
 * \brief append dynbuf with content of contents
 * \return Number of bytes appended
 */
size_t dynbuffer_write(const char *contents, dynbuffer *userp);

/**
 * \brief append dynbuffer with non null terminated string
 * \return number of bytes added
 */
size_t dynbuffer_write_n(const void *contents, size_t content_len, dynbuffer *userp);

/**
 * \brief create new dynbuffer object
 * \return the new object
 * \note the Caller is responsible for calling dynbuffer_clear() or dynbuffer_pop() to free the object
 */
dynbuffer * dynbuffer_init(void);

/**
 * \brief Create a new dynbuffer object with a given buffer
 * \param ptr pointer to the buffer to use in dynbuffer object, `*ptr` returns NULL on success
 * \param n size of data to append, use 0 for null terminated string
 * \return the new object
 */
dynbuffer * dynbuffer_init_push( char ** ptr, size_t n);

/**
 * \brief pop the buffer and free the object.
 * \param userp pointer to the dynbuffer object.
 * \param buffer pointer to the buffer.
 * \param len size of buffer if not NULL.
 */
void dynbuffer_pop( dynbuffer **userp, char ** buffer, size_t * len);

/**
 * \brief returns a pointer to the buffer, const char * for safty
 * \param userp dynbuf object
 * \return const char * of buffer
 */
const char * dynbuffer_get(dynbuffer *userp);

/**
 * \brief get the size of the buffer
 * \param userp dynbuf object
 * \return size of the buffer
 */
size_t dynbuffer_len(dynbuffer*userp);


/**
 * \brief free the dynbuf object
 * \param userp pointer to dynbuffer object
 * \note object pointer is returned as NULL.
 */
void dynbuffer_clear(dynbuffer **userp);


// Functions for IO_Stream

/**
 * \brief ungetc-implementation for IO_Stream
 * \param c Char to unget().
 * \param userp dynbuf object
 * \return 0 on success
 */
int dynbuffer_ungetc(int c, dynbuffer *userp);

/**
 * \brief getc-implementation for IO_Stream
 * \param userp dynbuf object
 * \return next char from dynbuffer or -1 on EOF or Error
 */
int dynbuffer_getc(dynbuffer *userp);

/**
 * \brief getc-implementation for IO_Stream
 * \param userp dynbuf object
 * \return 0 on success
 */
int dynbuffer_putc(int c,dynbuffer *userp);

/**
 * \brief fwrite-implementation for IO_Stream with dynbuffer
 * \param p non null terminated const string
 * \param s2 amount of blocks with size s1
 * \param s1 size of s2 blocks
 * \param userp dynbuf object
 * \return number of bytes writen, 0 on error
 */
size_t dynbuffer_fwrite(const void * p, size_t s2, size_t s1,dynbuffer *userp);

/**
 * \brief fread-implementation for IO_Stream with dynbuffer
 * \param p buffer
 * \param s2 amount of blocks with size s1
 * \param s1 size of s2 blocks
 * \param userp dynbuf object
 * \return number of bytes read, 0 on EOF
 */
size_t dynbuffer_fread( void * p, size_t s2, size_t s1,dynbuffer *userp);

/**
 * \brief writes const string to dynbuffer object
 * \param p string to write
 * \param userp dynbuf object
 * \return 0 on success
 */
int dynbuffer_fputs(const char * p,dynbuffer *userp);

//TODO: Add frewind


#endif /* defined(__deployctl__dyn_buffer__) */
