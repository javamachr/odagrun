//
//  curl_share.h
//  oc-runner
//
//  Created by Danny Goossen on 16/1/19.
//  Copyright (c) 2019 Danny Goossen. All rights reserved.
//

#ifndef __oc_runner__curl_share__
#define __oc_runner__curl_share__

#include <curl/curl.h>

enum sharing_conn {
	sharing_conn_none=0,
	sharing_conn_on
};

void curl_share_connect_init(void);
void curl_share_connect_free(void);

void curl_easy_set_shared_connection(CURL * curl,enum sharing_conn share_tpy);

#endif /* defined(__oc_runner__curl_share__) */
