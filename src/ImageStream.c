/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.
 
 This file is part of the odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

/**
 * \file ImageStream.c
 * \brief odagrun commands openshift ImageStream api
 * \author Created by Danny Goossen on  9/8/18.
 * \copyright 2018, Danny Goossen. MIT License
 */


#include "deployd.h"
#include "ImageStream.h"
#include "dyn_trace.h"
#include "cJSON_deploy.h"
#include "image_parameters.h"
#include "gitlab_api.h"
#include "specials.h"
#include "utils.h"
#include "dkr_api.h"
#include "oc_api.h"
#include "error.h"

/**
 \brief internal function to prepare and initialize the dkr ImageStream registry api
 
 this function calculates image details (registry name, image_name and reference),
 initializes the registry api
 and adds the registry to the registry database
 posible commandline options:
 * image_name
 * reference
 And thus to be ready for api actions towards the openshift IMAGESTREAM.
 
 parameter->imagename is compiled as 'is-<gitlab project path>[-image_name]'
 
 parameters->image as <IMAGESTREAM_IP>/<ODAGRUN_NAMESPACE>/is-<gitlab project path>[-image_name][:reference]
 
 parameters->reference as <reference> or latest is <reference> is not defined
 
 parameters->registry_name as <IMAGESTREAM_IP>
 
 <IMAGESTREAM_IP> is read from the environment, if not defined defaults to 172.30.1.1:5000
 
 \param job info
 \param parameters docker registry image parameters
 \param api_data returns pointer to api_data docker api data structure
 \param oc_api_data pointer to oc_api data structure
 \param argc argument count \b argv
 \param argv variable arguments passed from command line in script
 \return 0 on success
 \note caller is responsible for freeing \b api_data
 */
int prep_ISR_registry(const cJSON * job, dkr_parameter_t * parameters,struct dkr_api_data_s ** api_data,struct oc_api_data_s **oc_api_data,size_t argc,char*const*argv);




int ImageStream_delete(const cJSON * job,struct trace_Struct * trace,size_t argc,char*const*argv)
{
	struct dkr_api_data_s * apidata=NULL;
	dkr_parameter_t * parameters=calloc(1, sizeof(struct dkr_parameter_s));
	
	struct oc_api_data_s *oc_api_data=NULL;
	int res=prep_ISR_registry(job, parameters, &apidata, &oc_api_data,argc, argv);
	if (res==-1)
	{
		Write_dyn_trace(trace, red, "\nError command line options\n");
		update_details(trace);
	}
	
	
	Write_dyn_trace(trace, white, "\n   DELETE: \n");
	change_image_tag(&parameters->image_nick,NULL);
	Write_dyn_trace(trace, cyan, "\t%s\n",parameters->image_nick);
	
	Write_dyn_trace_pad(trace, yellow, 60, "\n   * oc_delete ...");
	//prep_oc_registry(env_vars, parameters, &oc_api_data, argc, argv);
	
	// Delete options "{\"gracePeriodSeconds\" : 0}"
	cJSON * deleteoptions=cJSON_CreateObject();
	cJSON_AddNumberToObject(deleteoptions, "gracePeriodSeconds", 10);
	
	
	const cJSON * sys_vars=cJSON_GetObjectItem(job ,"sys_vars");
	const char * ODAGRUN_NAMESPACE=cJSON_get_key(sys_vars,"ODAGRUN-OC-NAMESPACE");
	const char * oc_image_name=parameters->namespace+strlen(ODAGRUN_NAMESPACE)+1;
	cJSON* result=NULL;
	res=oc_api(oc_api_data, "DELETE", deleteoptions, &result, "/oapi/v1/namespaces/%s/imagestreams/%s",ODAGRUN_NAMESPACE,oc_image_name);
	if (!res) // deleted
	{
		Write_dyn_trace(trace, green, "  [OK]\n");
		res=0; // pushed
	}
	else if (result)
	{
		Write_dyn_trace(trace, red, "[FAIL]\n");
		Write_dyn_trace(trace, magenta,"\n   Status  :%s\n",cJSON_get_key(result, "status"));
		Write_dyn_trace(trace, magenta,  "   Reason  :%s\n",cJSON_get_key(result, "reason"));
		Write_dyn_trace(trace, magenta,  "   Message :%s\n",cJSON_get_key(result, "message"));
	}
	else
		print_api_error(trace, res);
	
	if (result) cJSON_Delete(result);
	
	oc_api_cleanup(&oc_api_data);
	if(res && parameters->allow_fail)
	{
		Write_dyn_trace(trace, yellow, " Warning: allowed to fail, exit success\n");
		res=0;
	}
	clear_dkr_parameters(&parameters);
	dkr_api_cleanup(&apidata);
	return res;
}

int ImageStream_delete_tag(const cJSON * job,struct trace_Struct * trace,size_t argc,char*const*argv)
{
	struct dkr_api_data_s * apidata=NULL;
	dkr_parameter_t * parameters=calloc(1, sizeof(struct dkr_parameter_s));
	
	struct oc_api_data_s *oc_api_data=NULL;
	int res=prep_ISR_registry(job, parameters, &apidata, &oc_api_data,argc, argv);
	if (res==-1)
	{
		Write_dyn_trace(trace, red, "\nError command line options\n");
		update_details(trace);
	}
	
	
	Write_dyn_trace(trace, white, "\n   DELETE TAG: \n");
	if (!parameters->reference)
	{
		parameters->reference=strdup("latest"); // for delete
		change_image_tag(&parameters->image_nick,"latest"); // for display
	}
	
	Write_dyn_trace(trace, cyan, "\t%s\n",parameters->image_nick);
	
	Write_dyn_trace_pad(trace, yellow, 60, "\n   * oc_delete ...");
	//prep_oc_registry(env_vars, parameters, &oc_api_data, argc, argv);
	
	// Delete options "{\"gracePeriodSeconds\" : 0}"
	cJSON * deleteoptions=cJSON_CreateObject();
	cJSON_AddNumberToObject(deleteoptions, "gracePeriodSeconds", 10);
	
	
	const cJSON * sys_vars=cJSON_GetObjectItem(job ,"sys_vars");
	const char * ODAGRUN_NAMESPACE=cJSON_get_key(sys_vars,"ODAGRUN-OC-NAMESPACE");
	const char * oc_image_name=parameters->namespace+strlen(ODAGRUN_NAMESPACE)+1;
	debug("Delete ocapi nameSpace/image:tag: %s/%s:%s\n",ODAGRUN_NAMESPACE,oc_image_name,parameters->reference);
	cJSON* result=NULL;
	//res=oc_api(oc_api_data, "DELETE", deleteoptions, &result, "/oapi/v1/namespaces/%s/imagestreams/%s",ODAGRUN_NAMESPACE,oc_image_name);
	
	//TODO: check if sha256 and remove the :, replace by @ !!!!!
	
	//TODO: copimage should return digest [0|1], images have different ways of displaying the digest e.g. sha2:<> or sha1:<> or <>
	//REMARK: not here delete tag is delete tag not delete digest !!!!!
	
	if (strstr(parameters->reference,"sha256:")==parameters->reference && strlen(parameters->reference)==71)
		res=oc_api(oc_api_data, "DELETE", NULL, &result, "/oapi/v1/namespaces/%s/imagestreamtags/%s%%40%s",ODAGRUN_NAMESPACE,oc_image_name,parameters->reference);
	else
		res=oc_api(oc_api_data, "DELETE", NULL, &result, "/oapi/v1/namespaces/%s/imagestreamtags/%s%%3A%s",ODAGRUN_NAMESPACE,oc_image_name,parameters->reference);
	if (!res) // deleted
	{
		Write_dyn_trace(trace, green, "  [OK]\n");
		res=0; // pushed
	}
	else if (result)
	{
		Write_dyn_trace(trace, red, "[FAIL]\n");
		Write_dyn_trace(trace, magenta,"\n   Status  :%s\n",cJSON_get_key(result, "status"));
		Write_dyn_trace(trace, magenta,  "   Reason  :%s\n",cJSON_get_key(result, "reason"));
		Write_dyn_trace(trace, magenta,  "   Message :%s\n",cJSON_get_key(result, "message"));
	}
	else
		print_api_error(trace, res);
	if (result) cJSON_Delete(result);
	
	oc_api_cleanup(&oc_api_data);
	if(res && parameters->allow_fail)
	{
		Write_dyn_trace(trace, yellow, " Warning: allowed to fail, exit success\n");
		res=0;
	}
	update_details(trace);
	clear_dkr_parameters(&parameters);
	dkr_api_cleanup(&apidata);
	return res;
}

int prep_ISR_registry(const cJSON * job, dkr_parameter_t * parameter,struct dkr_api_data_s ** api_data,struct oc_api_data_s **oc_api_data,size_t argc,char*const*argv)
{
	if ((optind=process_options(argc, argv,parameter)) < 0)
	{
		error(" *** ERRORin command line options, exit \n");
		return (-1);
	}
	if (argc > 1 && optind <= (int)argc) {
		alert("ignoring non option arguments\n");
	}
	if(parameter->archive==NULL)
	{
		parameter->archive=create_stringf("layer.tar.gz");
	}
	cJSON * env_vars=cJSON_GetObjectItem(job, "env_vars");
	if(parameter->image_dir==NULL)
	{
		parameter->image_dir=create_stringf("%s/image",cJSON_get_key(env_vars, "CI_PROJECT_DIR"));
	}
	if (! parameter->verbose && !parameter->quiet) parameter->verbose = VERBOSE_YN;

	set_ISR_registry(job, parameter, api_data, oc_api_data);
	return 0;
}


