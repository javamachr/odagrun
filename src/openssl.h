/*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the odagrun
 
 MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 
*/

/*! @file openssl.h
 *  @brief header for the openssl api
 *  @author danny@gioxa.com
 *  @date 9 Sep 2017
 *  @copyright (c) 2017 Danny Goossen,Gioxa Ltd.
 */

#ifndef __deploy_runner__openssl__
#define __deploy_runner__openssl__

#include "common.h"
#include <openssl/crypto.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/sha.h>

/**
  \brief initialize thread safty for curl
  \see https://curl.haxx.se/libcurl/c/threadsafe.html
  \see https://curl.haxx.se/libcurl/c/opensslthreadlock.html
 */
int thread_setup(void);
/**
 \brief cleanup thread safety for curl
 \see https://curl.haxx.se/libcurl/c/threadsafe.html
 \see https://curl.haxx.se/libcurl/c/opensslthreadlock.html
 */
int thread_cleanup(void);

/**
 \brief buffer length for the docker blobsum digest
 */
#define SHA256_DIGEST_LENGTH_BLOBSUM ((SHA256_DIGEST_LENGTH*2)+8)
/**
 \brief buffer length for hash sha256 digest
 */
#define SHA256_DIGEST_LENGTH_HASH ((SHA256_DIGEST_LENGTH*2))


int calc_sha256_buff (const char* buff,int size, char output[65+7]);


int calc_sha256_digest_hash (const char* buff,int size, char output[65]);


/**
 \brief internal calc sha256 checksum to digest formt "sha256:<hash>"
 \param path file to calculate the sha256 sum
 \param output to copy result in
 \return 0 on success?
 \todo check return
 */
int calc_sha256 (const char* path, char output[65+7]);

int calc_sha256_e (const char* path, char output[65+7],int * len);



/**
 \brief internal convert "<hash>" to "sha256:<hash>"
 \param hash input hash
 \param outputBuffer to copy result in
 
 */
void sha256_digest_string (unsigned char hash[SHA256_DIGEST_LENGTH], char outputBuffer[SHA256_DIGEST_LENGTH_BLOBSUM]);

/**
 \brief internal convert 23 byte "<hash>" to hex string "<hash>"
 \param hash input hash
 \param outputBuffer to copy result in
 
 */
void sha256_digest_string_hash (unsigned char hash[SHA256_DIGEST_LENGTH], char outputBuffer[SHA256_DIGEST_LENGTH_HASH]);


#endif
