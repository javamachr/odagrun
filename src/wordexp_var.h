/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.

 This file is part of the odagrun

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */
/*! \file wordexp_var.h
 *  \briefexpand safely variables and commandlines
 *  \author Created by Danny Goossen on 23/11/17.
 *  \copyright 2018, Danny Goossen. MIT License
 *
 */


#ifndef __odagrun__wordexp__
#define __odagrun__wordexp__

#include <wordexp.h>
/**
 * \brief resolves bash variables in \a valuestring with specific env as per \a env_vars
 * \param output_buf expanded string, caller provides the buffer, with len \a buff_len
 * \param buff_len buffer size of \a output_buf
 * \param valuestring the input to expand (single word)
 * \return 0 on success, -1 on failure ??? need check
 * \note current environment is not affected (expanded in fork)
 */
int word_exp_var(char * output_buf,int buff_len, const char* valuestring ,const cJSON * env_vars);


/**
 \brief wordexpansion struct
 */
struct word_exp_var_s
{
	cJSON * args_obj; /**< cJSON string array of arguments and memory allocation of argv[x] */
	char*const* argv;/**< argv */
	int argc;/**< number of items in argv[] */
	char * errmsg;/**< error msg */
};
/**
 \brief wordexpansion struct
 */
typedef struct word_exp_var_s word_exp_var_t;

/**
 \brief free wordexpansion struct
 */
void word_exp_var_free(word_exp_var_t * wev);

/**
 * \brief resolves bash variables in \a valuestring with specific env as per \a env_vars
 * \param wev wordexpansion struct, caller provides and frees
 * \param valuestring the input to expand (single word)
 * \return 0 on success, -1 on failure ??? need check
 * \note current environment is not affected (expanded in fork)
 * \note caller needs to free output buffer
 */
int word_exp_var_dyn(word_exp_var_t * wev, const char* valuestring ,const cJSON * env_vars,const char * dir);

#endif /* defined(__odagrun__wordexp__) */
