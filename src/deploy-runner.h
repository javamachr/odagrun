/*! \file deploy-runner.h
 *  \brief header functions to setup and check the runners for jobs
 *  \author Created by Danny Goossen on 21/7/17.
 *  \copyright (c) 2017 Danny Goossen. All rights reserved.
 */

#ifndef deployctl_deploy_runner_h
#define deployctl_deploy_runner_h
#include "common.h"

/*! \fn int runners(parameter_t * parameters)
    \brief main loop for checking and executing runners
    \param parameters program parameters/settings
    \return 0 on success, only returns on signals
 */
int runners(parameter_t * parameters);

#endif
