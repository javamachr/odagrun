//
//  DockerHub.h
//  odagrun
//
//  Created by Danny Goossen on 19/7/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

#ifndef __odagrun__DockerHub__
#define __odagrun__DockerHub__

#include "common.h"
/**
 \brief  Set the docker short and long description of a docker Hub repository
 
 DockerHub api
 
 \code DockerHub_set_description[ --long_filename=<filename>][ --description=<sumary>][ --allow_fail]\endcode
 
 Defaults
 --long_filename: ./README.md
 --description: ${ODAGRUN_IMAGE_SUMMARY}
 
 \param job to execute
 \param trace the feedback structure for the build-trace in gitlab
 \param argc number of arguments
 \param argv the command line arguments from the build script
 \return 0 on success
 */
int DockerHub_set_description(const cJSON * job,struct trace_Struct * trace,size_t argc,char* const *argv);

/**
 \brief Delete a Repository from Docker Hub
 
 Delete on existing Repository on the DockerHub
 
 Command
 \code DockerHub_delete_repository --name=<name> [--allow_fail]\endcode
 
 \param job to execute
 \param trace the feedback structure for the build-trace in gitlab
 \param argc number of arguments
 \param argv the command line arguments from the build script
 \return 0 on success
 */
int DockerHub_delete_repository(const cJSON * job,struct trace_Struct * trace,size_t argc,char* const*argv);

/**
 \brief  Delete a image-tag from DockerHub registry
 
 Delete an existing image tag from DockerHub
 
 Command \code DockerHub_delete_tag --name=<name> --reference=<reference> [--allow_fail]\endcode
 
 \param job to execute
 \param trace the feedback structure for the build-trace in gitlab
 \param argc number of arguments
 \param argv the command line arguments from the build script
 \return 0 on success
 */
int DockerHub_delete_tag(const cJSON * job,struct trace_Struct * trace,size_t argc,char*const *argv);



#endif /* defined(__odagrun__DockerHub__) */
