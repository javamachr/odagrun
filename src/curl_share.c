/*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/*! @file curl_share.c
 *  @brief main of oc_executer
 *  @author danny@gioxa.com
 *  @date 16/1/19
 *  @copyright (c) 2018 Danny Goossen,Gioxa Ltd.
 */

#include <curl/curl.h>
#include <pthread.h>
#include "curl_share.h"


static pthread_mutex_t connlock;

static CURLSH *share;



static void lock_cb(CURL *handle, curl_lock_data data,
					curl_lock_access access, void *userptr)
{
	(void)access; /* unused */
	(void)userptr; /* unused */
	(void)handle; /* unused */
	(void)data; /* unused */
	pthread_mutex_lock(&connlock);
}

static void unlock_cb(CURL *handle, curl_lock_data data,
					  void *userptr)
{
	(void)userptr; /* unused */
	(void)handle;  /* unused */
	(void)data;    /* unused */
	pthread_mutex_unlock(&connlock);
}

static void init_locks(void)
{
	pthread_mutex_init(&connlock, NULL);
}

static void kill_locks(void)
{
	pthread_mutex_destroy(&connlock);
}

void curl_share_connect_init(void)
{
	init_locks();
	share = curl_share_init();
	curl_share_setopt(share, CURLSHOPT_SHARE, CURL_LOCK_DATA_CONNECT);
	curl_share_setopt(share, CURLSHOPT_SHARE, CURL_LOCK_DATA_DNS);
	curl_share_setopt(share, CURLSHOPT_LOCKFUNC, lock_cb);
	curl_share_setopt(share, CURLSHOPT_UNLOCKFUNC, unlock_cb);
}

void curl_share_connect_free(void)
{
	curl_share_cleanup(share);
	share=NULL;
	kill_locks();
}

void curl_easy_set_shared_connection(CURL * curl,enum sharing_conn share_tpy)
{
	//(void)curl;
	(void)share_tpy;
	//return;
	//curl_easy_setopt(curl, CURLOPT_UPLOAD_BUFFERSIZE, 16384);
	
	if (share_tpy!=sharing_conn_none)
	{
		curl_easy_setopt(curl, CURLOPT_SHARE, share);
		/* enable TCP keep-alive for this transfer */
  		curl_easy_setopt(curl, CURLOPT_TCP_KEEPALIVE, 1L);
		
 		 /* set keep-alive idle time to 120 seconds */
  		curl_easy_setopt(curl, CURLOPT_TCP_KEEPIDLE, 120L);
		
 		 /* interval time between keep-alive probes: 60 seconds */
 		 curl_easy_setopt(curl, CURLOPT_TCP_KEEPINTVL, 60L);
	}
	else
		curl_easy_setopt(curl, CURLOPT_SHARE, NULL);
}


