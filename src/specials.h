//
//  specials.h
//  odagrun
//
//  Created by Danny Goossen on 16/10/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#ifndef __odagrun__specials__
#define __odagrun__specials__

#include "common.h"

/**
 \brief change all non a-z, A-Z , - , 0-9 characters to -, with max len of 63, no sequential -, no start nor stop with -
 */
char * slug_it(const char * input_str, size_t len);

/**
 \brief resolve variables used in image
 */
int resolve_image_var(char ** image,const cJSON * env_vars);

/**
 \brief create a full image path name
 */
char * process_image_nick(char ** image,const cJSON * env_vars,const char * Imagestream_ip, const char * oc_name_space);


/**
 \brief change image string tag to a given tag
 */
int change_image_tag(char ** image, const char *new_tag);

/**
 \brief chop image into \b registry_name / \b namespace / \b and reference
 \param image the image name to parse
 \param registry_name the registry
 \param namespace name
 \param reference ref tag
 \return 0 on success, I guess
 */
int chop_image(const char *image,char ** registry_name,char **namespace,char **reference);

/**
 \brief set correct docker registry domain 
 */
void rename_docker_io_domain(char ** registry_name);

/**
 * \brief compair registry names of 2 images
 * \return 0 on equal and have diffeent namespaces, 1 on equal with eaual namesapce and -1 on different
 */
int compair_registry_imagename(const char *image_a, const char *image_b);

/**
 \brief chop namespace into \b reponamespace / \b reponame
 \param namespace the namespace to parse
 \param reponamespace in the registry
 \param reponame in the registry namespace
 \return 0 on success
 */
int chop_namespace(const char * namespace,char ** reponamespace, char**reponame);

/**
 \brief chop namespace and reverse last 2 items and slugit
 \param namespace name
 \returns refname on success, NULL on failure
 \note client responsible for freeing memory
 */
char * name_space_rev_slug(const char * namespace);

/**
 \brief chop namespace and reverse last 2 items
 \param namespace name
 \returns refname on success, NULL on failure
 \note client responsible for freeing memory
 */
char * name_space_rev(const char * namespace);

/**
 \brief loop file string recursivly in reverse and if a dir path has not been added, do a call fn(void * userp,char *path)
 */
void recursive_dir_extract(const char * file, size_t len, cJSON * list, void(*fn)(void * userp,char *path),void * userp );

/**
 \brief create an cJSON array from a comma separated list
 */
cJSON * comma_sep_list_2_json_array(const char * list);


/**
 \brief check if an env var would indicate true (case insensitive)
 \param name environment variable
 \returns 1 on true
 */
int env_var_is_true(const char *name);

/**
 \brief check if an env var would indicate false (case insensitive)
 \param name environment variable
 \returns 1 on false
 */
int env_var_is_false(const char *name);

char * resolve_const_vars(const char * s,const cJSON * env_vars);

/**
 \brief convert Mi and Gi values to long
 \param memorystr the string value to convert
 \returns long in bytes
 */
long memorystring2long(const char * memorystr);

/**
 \brief convert xxx(m) cpu values to long
 \param cpustr the string value to convert
 \returns long in milli cpu
 */
long cpustring2long(const char * cpustr);

/**
 \brief check if 403 is caused by resource shortage
 \param result cjson object with result
 \returns 1 if shortage, 0 is something else
 */
int process403pod(cJSON * result);

#endif /* defined(__odagrun__specials__) */
