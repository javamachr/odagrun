/*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/*! @file common.h
 *  @brief main systen header file
 *  @author danny@gioxa.com
 *  @date 15/10/17
 *  @copyright (c) 2017 Danny Goossen,Gioxa Ltd.
 */

#ifndef deployctl_common_h
#define deployctl_common_h

#define run_local 1

#ifndef GITVERSION
#define GITVERSION "10.0"
#endif

#ifndef PACKAGE
#define PACKAGE "odagrun"
#endif

#ifndef PACKAGE_VERSION
#define PACKAGE_VERSION "10.0"
#endif

#ifndef PACKAGE_STRING
#define PACKAGE_STRING "odagrun-mac-dev"
#endif

#ifndef JOBENVIRONMENTVAR
#define JOBENVIRONMENTVAR "JOBDATA"
#endif

/**
 \brief fd value for invalid socket
 */
#define INVALID_SOCKET -1
/**
 \brief fd value for error on socket
 */
#define SOCKET_ERROR -1

#ifndef DEFAULT_CA_BUNDLE
/**
 \brief default value for location ca-bundle.crt
 */
#define DEFAULT_CA_BUNDLE_DIR "/etc/pki/tls/certs"
#define DEFAULT_CA_BUNDLE  DEFAULT_CA_BUNDLE_DIR"/ca-bundle.crt"
//#define DEFAULT_CA_BUNDLE "/bin-runner/ca-bundle.crt"
#endif

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <err.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

#include <fcntl.h>
#include <sys/ioctl.h>
#include <errno.h>

#include <pwd.h>
#include <grp.h>
#include <ctype.h>

#include <string.h>
#include <strings.h>

#include <limits.h>

#include <syslog.h>
#include <pthread.h>
#include <signal.h>
#include <getopt.h>

#include <sys/socket.h>
#include <sys/resource.h>
#include <sys/un.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/termios.h>
#include <paths.h>

#include <dirent.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/param.h>
#include <time.h>

#include <stdbool.h>

#include <cJSON.h>

#include "Environment.h"

//#include <cmark.h>
#include <git2.h>
#include <yaml.h>


#include <zip.h>

#include <openssl/err.h>
#include <curl/curl.h>

#define PCRE2_CODE_UNIT_WIDTH 8
#include <regex.h>

#if HAVE_PTY_H==1
#include <pty.h>
#endif


#define       ANSI_BOLD_BLACK    "\033[30;1m"
#define       ANSI_BOLD_RED      "\033[31;1m"
#define       ANSI_BOLD_GREEN    "\033[32;1m"
#define       ANSI_BOLD_YELLOW   "\033[33;1m"
#define       ANSI_BOLD_BLUE     "\033[34;1m"
#define       ANSI_BOLD_MAGENTA  "\033[35;1m"
#define       ANSI_BOLD_CYAN     "\033[36;1m"
#define       ANSI_BOLD_WHITE    "\033[37;1m"
#define       ANSI_BLACK         "\033[30m"
#define       ANSI_RED           "\033[31m"
#define       ANSI_GREEN         "\033[32m"
#define       ANSI_YELLOW        "\033[33m"
#define       ANSI_BLUE          "\033[34m"
#define       ANSI_MAGENTA       "\033[35m"
#define       ANSI_CYAN          "\033[36m"
#define       ANSI_WHITE         "\033[37m"
#define       ANSI_RESET         "\033[0;m"
#define       ANSI_CLEAR         "\033[0K"
#define       ANSI_CROSS_BLACK   "\033[30;9m"
#define       ANSI_CROSS_RED     "\033[31;9m"
#define       ANSI_CROSS_GREEN   "\033[32;9m"
#define       ANSI_CROSS_YELLOW  "\033[33;9m"
#define       ANSI_CROSS_BLUE    "\033[34;9m"
#define       ANSI_CROSS_MAGENTA "\033[35;9m"
#define       ANSI_CROSS_CYAN    "\033[36;9m"
#define       ANSI_CROSS_WHITE   "\033[37;9m"

extern const char * color_data[];

/**
 \brief ANSI enum for standard terminal colors
 */
enum term_color { bold_black,bold_red,bold_green,bold_yellow,bold_blue,bold_magenta,bold_cyan,bold_white,black,red,green,yellow,blue,magenta,cyan,white,cross_black,cross_red,cross_green,cross_yellow,cross_blue,cross_magenta,cross_cyan,cross_white,none,reset};


/**
 \brief pointers to pass to the curl callback function to interupt a long polling
 \see see get_job()
 */
typedef struct stop_struct_s
{
    int * exit;
    int * reload;
} stop_struct_t;

/*------------------------------------------------------------------------
 * global application parameters
 *------------------------------------------------------------------------*/

/**
 \brief typedef parameter structure, \b  parameter_s
 */
typedef  struct parameter_s parameter_t;
/**
 \brief parameter structure, general program settings
 */
struct parameter_s
{
   u_int8_t verbose; /**< verbose output */
   u_int8_t quiet;   /**< quiet, no output */
   char * prefix;     /**< workdir prefix */
   char * config_file; /**< config file location */
	 char * service_dir; /**< location of the service account info */
   int debug;          /**< print debug information */
   int timeout;        /**< general timeout for actions */
	int shell_exec;    /**< shellexec */
   char * testprefix;
   int hubsignal;      /**< reload signal received */
   int exitsignal;     /**< exit signal received */
	char * ca_bundle;   /**< general ca_bundle */
   char * shell;       /**< shell if any */
	char digest_tar[65+7];
	char digest_tar_gz[65+7];
	char  digest_config[65+7];
	int config_size;
	int layer_size;
	int build_storage;  /**< if 1, separate volume is mounted for builds */
	int pipe_env; /**< executer will pipe env to pipe as serialised json*/
	int pipe_pwd; /**< executer will pipe PWD to pipe as serialised json*/
	int exit; /**< executer will exit with this code*/
	char * commandpath; /**< excact path how to start executor */
	int virgin; /**< indicates we start dispatcher in virgin mode*/
	int ready; /**< indicates we start dispatcher in check readiness mode*/
} ;



// struct exchange data for functions, not sure that belongs here, reevaluate
/**
 \brief wrapper to pass data to the exec
 */
typedef struct data_exchange_t
{
	Environment * env;          /**< the enviroment vars of the job, n*/
   parameter_t * parameters;    /**< the program parameters, not sure if still needed */
   const char * shellcommand;   /**< command to execute */
   struct trace_Struct * trace; /**< feedback trace data */
   int timeout;                 /**< stall timeout for exec */
   const char * commandpath; /**< excact path how to start executor */
 } data_exchange_t;

// for trace
/**
  \brief the data structure that holds all data for the feedback trace
 */
struct trace_Struct {
   cJSON *params;                  /**< the json parameters for feedback (success, reason fail, the trace, ...) */
   char * url;                     /**< gitlab url */
   const char * ca_bundle;         /**< ca_ bundle to communicate wth gitlab */
   int job_id;                     /**< job id */
   ssize_t mark;                    /**< mark a position in the buffer for delete, get, rewrite ..etc */
   void * tests;                   /**< dummy var, used for testing */
   CURL * curl;                    /**< the connection to gitlab */
   size_t trace_buf_size;          /**< memory size of the trace buffer */
   size_t min_trace_buf_increment; /**< when trace buf is to small for new data, minimun increase of the buffer size*/
   size_t trace_len;               /**< the length of the trace */
   size_t last_nl;                 /**< pos of last known <new line>*/
   size_t first_cr;                 /**< pos of active known <cariage_return>*/
   size_t prev_len;                 /**< previous length of trace, used for padding to force update*/
   size_t last_diff;
   int count;
};
/*
 return stderr in variable
 */
#define STDERR_REDIRECT int my_pipe[2]; \
my_pipe[0]=-1; \
my_pipe[1]=-1; \
int saved_stderr=pipe_redirect_stderr(my_pipe);

#define STDERR_UNDIRECT(X) pipe_redirect_undo(my_pipe, saved_stderr, X);

#endif
