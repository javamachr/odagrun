
## DockerHub API


<br>

### Purpose

Managing DockerHub Meta data and privacy settings from GitLab-CI job allows uniform and version controlled Meta Data without manual interactions.

Besides Meta Data, the API can also:

- delete Tags, and only Tags, not the references, allowing cleanup intermediate Tags without deleting tags refering to the same manifest.
- deleting a complete docker Repository.


### Credits

This would not have been posible without the [Docker Hub API](https://github.com/RyanTheAllmighty/Docker-Hub-API) from [Ryan Dowling](https://github.com/RyanTheAllmighty) since no official documentation has been released.


### Common options for DockerHub API


- `--image=namespace/image` need to be a valid docker image `repo-name`:
    - `centos`
    - `library/centos`
    - `registry.hub.docker.com/library/centos`

<br>
<div class="alert alert-info"><strong>Note:</strong> the tag for <code>--image</code> is ignored.<br>e.g.: <code>centos:latest</code> is valid too.</div>
<br>

- `--credentials=$DOCKER_CREDENTIALS`
  

    - Create base64 string credentials with a docker given `username` and `password`:
    ```bash
echo -e -n "myname:mypwd" | base64 -
```
    - verify this and it should return `myname:mypwd` with:
    ```bash
echo -e -n "dXNlcm5hbWU6cGFzc3dvcmQ=" | base64 -D  -
```

    - Alternatively, on a linux computer, extract the `DOCKER_CREDENTIALS` from `/root/.docker/config.json`, take the `auth` value from `index.docker.io` **after** a succesfull `sudo docker login <reponame>`:
    ```json
{
    "auths": {
        "https://index.docker.io/v1/": {
            "auth": "ZGdxxxxxxxxxxxxxxxxxxUhdw=="
        }
    }
}
```

Alternatively add a secret variable to the Gitlab-CI settings:


```
DOCKER_CREDENTIALS=ey...........
```

<br>

- optional [`--allow-fail`] will generate only a `Warning` in stead of failing the build.


<br>

### Set OR Create Docker Hub META data


```bash
DockerHub_set_description \
  --image=${repo-name} \
  [--credentials=$(base64(username:password))] \
  [--set-private[=yes|no|auto|none]] \
  [--full_description="my DockerHub long description"] \
  [--description="my description"] \
  [--allow-fail]
```

*Defaults:* with priority as listed

| Option | values |
|-------------:|:----------------------------------------|
|credentials| `DOCKER_CREDENTIALS` |
|full_<br>description|`ODAGRUN_IMAGE_DESCRIPTION`<br>`file:./description.md` <br>`file:./README.md`<br>|
|description|`ODAGRUN_IMAGE_TITLE`<br>`ODAGRUN_IMAGE_REFNAME`<br>`ODAGRUN_REVERSE_PROJECT_PATH`|
|set-private| auto |


`DockerHub_set_description` also allows to set the repository private:

| set-private | result |
|------:|---------------------------------|
| auto | set to **private** if Gitlab Project Visibility is `private` or `internal`<br> set to **public** if Gitlab Project Visibility is `public` |
| yes <br>`no options` | the Docker repository is set to **private**. <br> same as `--set-private` without options |
| no |  the Docker repository is set **public**. |
| none | the Docker repository private setting is **not altered**! |
| !`yes`<br>!`no`<br>!`none` | defaults to **auto** |


<br>

<div class="alert alert-info"><strong>Info!</strong><br>If the repository for the given image does not exists, it will be created, this allows to create a private repository before a registry_push, ensuring that a private image stays private!</div>

<br>

<div class="alert alert-warning"><strong>Warning!</strong> <br>The Project visibility of Gitlab is only available since Gitlab version <code>10.3</code> in CI, before <code>v10.3</code>, the <code>--set-private=auto</code> option will result in a public docker Repository.</div>

<br>

### Delete a Docker image Tag


```bash
DockerHub_delete_tag \
  [--allow-fail] \
  --image=${image} \
  --reference=${tag} \
  [--credentials=$DOCKER_CREDENTIALS]
```

Deletes a given tag for a given repository image.

<br>

### Delete a Docker repository


```bash
DockerHub_delete_repository \
  [--allow-fail] \
  --image=${image} \
  [--credentials=$(base64(username:password))]
```

Deletes a repository with given image name.

<div class="alert alert-danger"><strong>Danger!</strong> This deletes the repository with all tags and description on the DockerHub!</div>


[top](#odagrun)

