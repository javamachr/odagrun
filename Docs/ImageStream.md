
## ImageStream API

<br>

### ImageStream_Delete_Image

Delete an image on the integrated registry on an Openshift Cluster.

```bash
ImageStream_Delete_Image [--name=subname]
```

<br>
<div class="alert alert-info"><strong>Info!</strong> Not all images can be deleted, only those which starts as : <br><pre>${ODAGRUN_NAMESPACE}/is-$CI_PROJECT_PATH_SLUG[-subname]</pre></div>
<br>

### ImageStream_Delete_Tag



```bash
ImageStream_Delete_Tag [--name=name] --reference=tag
```

Delete an ImageStream tag, with the same restriction as with ImageStream_Delete_Image.


[top](#odagrun)

