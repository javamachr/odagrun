
## odagrun variables

For specific Gitlab-ci variables, please refer to the [GITLAB_CI](https://docs.gitlab.com/ce/ci/variables/)

### important variable protection:

the Following Gitlab-CI variables cannot be overwritten by user intervention, and are thus protectect readonly Gitlab-CI system defined:

- `CI_PROJECT_PATH`
- `CI_PROJECT_URL`
- `GITLAB_USER_NAME`
- `GITLAB_USER_EMAIL`
- `GITLAB_USER_ID`
- `GITLAB_USER_LOGIN`
- `CI_REPOSITORY_URL`
- `CI_REGISTRY`
- `CI_REGISTRY_IMAGE`
- `CI_PROJECT_DIR`


### custom defined by odagrun:

odagrun will set a couple of variables:

- `ODAGRUN_GIT_VERSION` as in `0.0.33-15-ge2b011bf` is not availlble when `GIT_STRATEGY_NONE` is set.

Info regarding the Build image used:

- `ODAGRUN_BUILD_IMAGE`
- `ODAGRUN_BUILD_IMAGE_NICK`
- `ODAGRUN_BUILD_IMAGE_TAG`
- `ODAGRUN_BUILD_IMAGE_SHA`


General purpose provided variables:

- `ODAGRUN_COMMIT_TAG_SLUG` a tag in the form of e.g. `3-1-9`, from the `CI_COMMIT_TAG`: `3.1.9`
-  `ODAGRUN_REVERSE_PROJECT_PATH*` from e.g.: `odagrun/build-image` :
    - `ODAGRUN_REVERSE_PROJECT_PATH_SLUG` => `build-image-odagrun`(*)
    - `ODAGRUN_REVERSE_PROJECT_PATH`      => `build-image odagrun` (*)
- `ODAGRUN_RFC3339_DATE` e.g. `2018-08-12T08:23:23Z`
- `ODAGRUN_SHORT_DATE`   e.g. `20180810`
- `nl` is set to `\n` as setting a bash variable with newline is a bit tricky without the use of `echo`

*Note:* if `CI_PROJECT_PATH` contains more then 1 slash, only the last 2 elements are chosen, if the last 2 elements are equal, only the last element is chosen, e.g.:
    
- gioxa/odagrun/build-image => `build-image odagrun`
- gioxa/odagrun/odagrun => `odagrun`
- odagrun/build-image => `build-image odagrun`

### For autolabeling

registry_push will automatic append labels to the pushed image according to the [org.opencontainer.image](https://github.com/opencontainers/image-spec/blob/master/annotations.md#pre-defined-annotation-keys) labeling scheme.

**Priorities:**

All variables defined in `docker_config.yml` proceed the environment variables

1. docker_config.yml:

    1. `org.opencontainers.image.xxx`
    2. `xxx`
    3. additonal `maintainer` as alternative for `org.opencontainers.image.authors`
    4. additional `license` as alternative for `org.opencontainers.image.licenses`

2. environment
    - `ODAGRUN_IMAGE_XXXX`

3. exeptions: the following labels in `docker_config.yml` are **ignored**
    - `org.opencontainers.image.source`
    - `org.opencontainers.image.revision`
    - `org.opencontainers.image.ref.name`

4. `org.opencontainers.image.version`,searched in this order:
    1.  file `docker_config.yml` label: `org.opencontainers.image.version`
    2.  file `docker_config.yml` label: `version`
    3.  environment var: `ODAGRUN_IMAGE_VERSION`
    3.  environment var: `ODAGRUN_GIT_VERSION`
    4.  environment var: `GIT_VERSION`
    5.  environment var: `GITVERSION`
    9.  read-file `$CI_PROJECT_DIR/.VERSION`
    6.  read-file `$CI_PROJECT_DIR/.GIT_VERSION`
    7.  read-file `$CI_PROJECT_DIR/.GITVERSION`
    8.  read-file `$CI_PROJECT_DIR/.tarball-version`


5. `org.opencontainers.image.ref.name`, label in `docker_config.yml` is **ignored**
	- namepart:
        1. environment var : `ODAGRUN_IMAGE_REFNAME`
        2. slug( environment var: `ODAGRUN_IMAGE_TITLE`)
        3. reverse slug of last 2 sections of `CI_PROJECT_PATH` e.g.:
            - `gioxa/odagrun/image` becomes `image-odagrun`
            - `gioxa/odagrun/odagrun` becomes `odagrun`
    - version-part as per above point 4

6. `org.opencontainers.image.title` *if not defined in docker_config.yml*
    1. `ODAGRUN_IMAGE_TITLE`
	2. `ODAGRUN_IMAGE_NAME`
    3. reverse of last 2 sections of `CI_PROJECT_PATH` e.g.:
        - `gioxa/odagrun/image` becomes `image odagrun`
        - `gioxa/odagrun/odagrun` becomes `odagrun`

7.  `org.opencontainers.image.authors` *if not defined in docker_config.yml*
    Read from `$CI_PROJECT_DIR/AUTHORS`, but all `\n` removed (flattened)


#### variables used for label schema

- Custom defined by user variables: *if not defined in docker_config.yml*

    | Label | Variable | note |
| --- | --- | --- |
| org.opencontainers.image.title | `ODAGRUN_IMAGE_TITLE` | (1) |
| org.opencontainers.image.licenses | `ODAGRUN_IMAGE_LICENSES` | |
| org.opencontainers.image.vendor | `ODAGRUN_IMAGE_VENDOR` | |
| org.opencontainers.image.description | `ODAGRUN_IMAGE_DESCRIPTION` | (2) |
| org.opencontainers.image.documentation | `ODAGRUN_IMAGE_DOCUMENTATION` | |
| org.opencontainers.image.url   | `ODAGRUN_IMAGE_URL` | |

    **note**

        1. Also used as default description of `DockerHub_set_description`
        2. Also used as default description of `DockerHub_set_description`

- odagrun provided variables: *if not defined in docker_config.yml*

    | Label | Variable |
| --- | --- |
| org.opencontainers.image.version |    `ODAGRUN_IMAGE_VERSION` |


- gitlab-ci variables

    | Label | Variable |
| --- | --- |
| org.opencontainers.image.source |   `CI_PROJECT_URL` |
| org.opencontainers.image.revision |   `CI_COMMIT_SHA` |

- `com.odagrun.image`

    | Label | Variable |
| --- | --- |
| com.odagrun.build.image | `ODAGRUN_BUILD_IMAGE` |
| com.odagrun.build.image-nick | `ODAGRUN_BUILD_IMAGE_NICK` |
| com.odagrun.build.image-tag | `ODAGRUN_BUILD_IMAGE_TAG` |
| com.odagrun.build.image-digest | `ODAGRUN_BUILD_IMAGE_SHA` |

- `com.odagrun.build`

    | Label | Variable |
| --- | --- |
| com.odagrun.build.commit.author | `"%s <%s>", GITLAB_USER_NAME, GITLAB_USER_EMAIL` |
| com.odagrun.build.commit.id | `CI_COMMIT_SHA` |
| com.odagrun.build.commit.message | `CI_COMMIT_MESSAGE` |
| com.odagrun.build.commit.ref | `CI_COMMIT_REF_NAME` |
| com.odagrun.build.commit.tag | `CI_COMMIT_TAG` |
| com.odagrun.build.version | `ODAGRUN_GIT_VERSION` |
| com.odagrun.build.environment | `CI_ENVIRONMENT_NAME` |
| com.odagrun.build.job-url | `"%s/-/jobs/%s", CI_PROJECT_URL, CI_JOB_ID` |




### Credentials

support for user provided secret variables:

- `DOCKER_CREDENTIALS` for **api** and **repository** access Docker Hub
- `QUAY_CREDENTIALS` for **repository** access `quay.io`
- `QUAY_OAUTH_TOKEN` for **repository** and **api** access `quay.io`

How to get the Credentials:

- Create base64 credentials string  with a given username and password for the repository:

```bash
echo -e -n "myname:mypwd" | base64 -
```

verify this and it should return myname:mypwd with:

```bash
echo -e -n "dXNlcm5hbWU6cGFzc3dvcmQ=" | base64 -D  -
```

- Alternatively, on a linux computer, extract the CREDENTIALS from `/root/.docker/config.json`, take the auth value from e.g. `index.docker.io` after a succesfull `sudo docker login <reponame>`:

```json
{
  "auths": {
              "https://index.docker.io/v1/": {
              "auth": "ZGdxxxxxxxxxxxxxxxxxxUhdw=="
			  }
		   }
}
```

### Debugging

Set `ODAGRUN_DEBUG` to True will enable debug output for the runner.

*Remark:*

If dispatcher is run with `--debug` option, executer will be started with `--debug`


[top](#odagrun)


